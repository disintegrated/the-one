package test;

import cmrra.entity.MapFeature;
import com.vividsolutions.jts.geom.*;
import core.Coord;

import cmrra.utility.geometry.*;
import org.javatuples.Pair;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;
import static org.junit.Assert.*;
import com.vividsolutions.jts.io.*;

/**
 * Created by rabbiddog on 09.08.17.
 */
public class GeometryUtilTest {


    @Test
    public  void getSweepingWaypointTest()
    {
        Coordinate[] cord = new Coordinate[5];
        cord[0] = new Coordinate(10000 ,40000);
        cord[1] = new Coordinate(10000, 90000);
        cord[2] = new Coordinate( 40000 ,120000);
        cord[3] = new Coordinate( 40000 ,20000);
        cord[4] = new Coordinate(10000,40000);
        Geometry poly = GeometryUtil.GEOMETRY_FACTORY.createPolygon(cord);
        MapFeature feature = new MapFeature("TestFeature", poly, null);
        LinkedList<Coordinate> waypoints = GeometryUtil.getSweepingWaypoint(poly, 1000);
        Coordinate[] ar = new Coordinate[waypoints.size()];
        ar = waypoints.toArray(ar);
        LineString ls = GeometryUtil.GEOMETRY_FACTORY.createLineString(ar);
        WKTWriter wr = new WKTWriter();
        String answer = wr.write(ls);
    }


    @Test
    public  void getSweepingWaypointPipeLineTest()
    {
        try
        {
            WKTReader rd= new WKTReader();
            Geometry g = rd.read("POLYGON ((173120.48762840440031141 242394.00115821856888942, 213686.01948782481485978 245800.26719221571693197, 265089.67054632701911032 161262.93743937771068886, 173120.48762840440031141 242394.00115821856888942))");
            MapFeature feature = new MapFeature("TestFeature", g, null);
            LinkedList<Coordinate> waypoints = GeometryUtil.getSweepingWaypoint(g, 3700);
            Coordinate[] ar = new Coordinate[waypoints.size()];
            ar = waypoints.toArray(ar);
            LineString ls = GeometryUtil.GEOMETRY_FACTORY.createLineString(ar);
            WKTWriter wr = new WKTWriter();
            String answer = wr.write(ls);
        }catch (ParseException pex)
        {

        }

    }

}
