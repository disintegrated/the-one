package test;

import com.vividsolutions.jts.geom.*;
import core.Coord;

import cmrra.utility.geometry.*;
import org.javatuples.Pair;
import org.junit.Test;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import static org.junit.Assert.*;
/**
 * Created by rabbiddog on 24.07.17.
 */
public class CMRRAWktMapReaderTest{

    @Test
    public void parsePolygonsWithPolygonMapValueTest(){

        WktMapParser parser = new WktMapParser();

        ConcurrentHashMap<String, Pair<ArrayList<Coord>, Geometry>> features =parser.parseMapFeatures("data/cmrra/PolygonTest1.wkt", '|');

        assertTrue(features.get("region1") != null);

    }

}
