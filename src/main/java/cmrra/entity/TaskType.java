package cmrra.entity;

import core.Coord;

/**
 * Created by rabbiddog on 29.07.17.
 */
public enum TaskType {

    SURVEY,            //systematically covers a region. Task is to be done only once
    CONTACT_HOST,      //should go to the las known location or region of host on path with least resistance and search.Task is to be done onyl once
    RECHARGE_BATTERY,       //should go to the nearest station.Task is to be done only once
    SEARCH,       //Search a region for a host at regular interval
    WANDER
}
