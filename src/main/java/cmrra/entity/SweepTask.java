package cmrra.entity;

import core.Coord;
import core.DTNHost;
import movement.Path;

import java.util.*;

/**
 * Created by rabbiddog on 01.08.17.
 * This is a concrete task class that is used to assign surveillance tasks
 * A feature Id is required
 * During survey allow contact and collection of data form other hosts
 */
public class SweepTask extends HostTask {

    public String featureId;

    //plan the path already in planning
    public LinkedList<Coord> path;

    public HashMap<String, Coord> subTasks;

    public SweepTask(TaskType actionType, String unqId, String featureId, LinkedList<Coord> path)
    {
        super(actionType, unqId);
        this.featureId = featureId;
        this.path = path;
        subTasks = new HashMap<>();
    }

    @Override
    public void setDone()
    {
        this.taskState = HostTaskState.COMPLETED;
    }

    @Override
    public void abort()
    {
        this.taskState = HostTaskState.ABORTED;
    }

    @Override
    public void pause()
    {
        this.taskState = HostTaskState.PAUSED;
    }

    public void addSubTask(Coord c, HostTask ht)
    {
       subTasks.put(ht.unqId, c); //add or update
    }
}
