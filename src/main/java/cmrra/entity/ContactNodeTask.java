package cmrra.entity;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import core.Coord;
import core.SimError;

import java.util.HashMap;

/**
 * Created by rabbiddog on 01.08.17.
 * This is the Counterpart of the SearchTask where we provide an exact location instead of a region.
 * The node stays at the location waiting for a contact or other task
 */
public class ContactNodeTask extends HostTask
{
    public Geometry expectedLoc;

    //time to spend on the task
    public double duration;

    //identifier of the host searching for
    public String target;

    public Coord onWayTo;

    public ContactNodeTask(TaskType actionType, String unqId, String target, Geometry expectedLoc, double duration)
    {
        super(actionType, unqId);
        this.target = target;
        if(expectedLoc instanceof Polygon || expectedLoc instanceof Point)
        {
            this.expectedLoc =expectedLoc;
        }
        else
        {
            throw new SimError("ContactNodeTask expects either of Polygon or Point as expected location. Found: "+expectedLoc.getClass());
        }
        this.duration = duration;
    }

    @Override
    public void setDone()
    {
        taskState = HostTaskState.COMPLETED;
    }

    @Override
    public void abort()
    {
        taskState = HostTaskState.ABORTED;
    }

    @Override
    public void pause()
    {
        taskState = HostTaskState.PAUSED;
    }
}
