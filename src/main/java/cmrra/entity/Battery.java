package cmrra.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by rabbiddog on 09.08.17.
 */
public class Battery {
    private double _flightTime; //in simtime
    private double _rechargeTime; //in simtime
    private double _batteryLevel;
    protected final Logger _log;
    protected String TAG = "Battery";
    protected final String hostId;
    public Battery(double flightTime, double rechargeTime, String hostId)
    {
        this._flightTime =flightTime;
        this._rechargeTime = rechargeTime;
        this._batteryLevel = flightTime;
        this.hostId = hostId;
        _log = LogManager.getRootLogger();
        TAG = TAG + hostId+": ";
    }

    public void runFor(double time)
    {
        _batteryLevel -= time;
        if(_batteryLevel < 0.0){
            _batteryLevel = 0.0;
            _log.info(TAG + "Battery ran out before reaching home. Ignoring issue");
        }

    }

    public void rechargeFor(double time)
    {
        _batteryLevel += ((_flightTime/_rechargeTime)*time);
        if(_batteryLevel > _flightTime)
            _batteryLevel = _flightTime;
    }

    public double get_batteryLevel() {
        return _batteryLevel;
    }

    public boolean isBatteryFull()
    {
        if((_flightTime - _batteryLevel) < 0.00009)
            return true;
        else
            return false;
    }

    public double batteryPercentage()
    {
        return (_batteryLevel*100)/_flightTime;
    }
}
