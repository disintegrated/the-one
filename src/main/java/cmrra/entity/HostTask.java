package cmrra.entity;

import core.Coord;
import core.SimClock;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by rabbiddog on 29.07.17.
 */
public abstract class HostTask {

    public TaskType taskType;

    public final String unqId;

    //public double stateChangedAtTime;
    public HostTaskState taskState;

    public HostTask(TaskType actionType, String unqId)
    {
        this.taskType = actionType;
        this.unqId = unqId;
        this.taskState = HostTaskState.CREATED;
        //this.stateChangedAtTime = SimClock.getTime();
    }
/*
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof HostTask)) {
            return false;
        }

        HostTask that = (HostTask) other;

        // Custom equality check here.
        return (this.unqId.compareTo(that.unqId) == 0);
    }

    @Override
    public int hashCode() {
        int hashCode = 1;

        hashCode = hashCode * 37 + this.unqId.hashCode();

        return hashCode;
    }*/

    public static String getHashId()
    {
        UUID id = UUID.randomUUID();
        return id.toString();
    }

    public abstract void setDone();

    public abstract void abort();

    public abstract void pause();
}
