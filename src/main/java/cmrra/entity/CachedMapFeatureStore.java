package cmrra.entity;

import core.Coord;
import movement.map.MapNode;
import movement.map.SimMap;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by rabbiddog on 25.07.17.
 */
public class CachedMapFeatureStore {

    private static CachedMapFeatureStore _instance;

    protected ConcurrentHashMap<String, MapFeature> store;

    private Hashtable<Coord, MapNode> nodes;
    private boolean nodeHashValid;
    /** are all paths bidirectional */
    private boolean bidirectionalPaths = true;
    private int nodeType = -1;

    private CachedMapFeatureStore()
    {
        store = new ConcurrentHashMap<>();
        nodeHashValid = false;
        nodes = new Hashtable<>();
    }

    public static CachedMapFeatureStore Instance()
    {
        if(null == _instance)
        {
            _instance = new CachedMapFeatureStore();
        }

        return _instance;
    }

    public MapFeature addFeatureToStore(String featureId, MapFeature feature)
    {
        if(!store.containsKey(featureId))
        {
            store.put(featureId, feature);
            nodeHashValid = false;
            return null;
        }
        else
            return store.get(featureId);
    }

    public MapFeature getFeature(String featureId)
    {
        if(store.containsKey(featureId))
        {
            return store.get(featureId);
        }
        else
            return null;
    }

    public boolean isFeatureInStore(String featureId)
    {
        return store.containsKey(featureId);
    }

    /**
     * Returns new a SimMap that is based on the read maps
     * @return new a SimMap that is based on the read maps
     */
    public SimMap getMap()
    {
        if(nodeHashValid)
            return new SimMap(this.nodes);
        else
        {
            //clear old data
            nodes.clear();
            for (MapFeature mapFeature: store.values())
            {
                    updateMap(mapFeature.coords());
            }
            nodeHashValid = true;
            return new SimMap(this.nodes);
        }
    }

    public SimMap getMapFromFeatureList(ArrayList<String> featureIds)
    {
        return null;
    }

    /**
     * Updates simulation map with coordinates in the list
     * @param coords The list of coordinates
     */
    private void updateMap(List<Coord> coords) {
        MapNode previousNode = null;
        for (Coord c : coords) {
            previousNode = createOrUpdateNode(c, previousNode);
        }
    }

    /**
     * Creates or updates a node that is in location c and next to
     * node previous
     * @param c The location coordinates of the node
     * @param previous Previous node whose neighbor node at c is
     * @return The created/updated node
     */
    private MapNode createOrUpdateNode(Coord c, MapNode previous) {
        MapNode n = null;

        n = nodes.get(c);	// try to get the node at that location

        if (n == null) { 	// no node in that location -> create new
            n = new MapNode(c);
            nodes.put(c, n);
        }

        if (previous != null) {
            n.addNeighbor(previous);
            if (bidirectionalPaths) {
                previous.addNeighbor(n);
            }
        }

        if (nodeType != -1) {
            n.addType(nodeType);
        }

        return n;
    }
}
