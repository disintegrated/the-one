package cmrra.entity.data;

import java.util.List;

public interface ONData {
    void addToPath(String nodeID);
    List<String> getPath();
}
