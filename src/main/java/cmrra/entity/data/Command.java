package cmrra.entity.data;

import cmrra.data.index.entity.Spatial2DBiTemporalData;
import cmrra.data.index.entity.SpatialAxisInterval;
import cmrra.data.index.entity.TransactionTime;
import cmrra.data.index.entity.ValidTime;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;

import java.util.ArrayList;
import java.util.List;

public class Command extends Spatial2DBiTemporalData<String> implements ONData {

    private String _cmd;
    private List<String> path;

    public Command(String UUID, Sp2DBiTemporalBoundary boundary, String cmd) {
        super(UUID, boundary);
        this._cmd = cmd;
        path = new ArrayList<>();
    }

    public String getCmd() {
        return _cmd;
    }

    @Override
    public SpatialAxisInterval X() {
        return _boundary.X();
    }

    @Override
    public SpatialAxisInterval Y() {
        return _boundary.Y();
    }

    @Override
    public TransactionTime T() {
        return _boundary.T();
    }

    @Override
    public ValidTime V() {
        return _boundary.V();
    }

    @Override
    public Sp2DBiTemporalBoundary BB() {
        return _boundary;
    }

    @Override
    public boolean hasHidden() {
        return _boundary.hasHidden();
    }

    @Override
    public void delete()
    {
        _boundary.delete(); //sets the transaction and valid time
        _deleted = true;
    }

    @Override
    public void addToPath(String nodeID) {
        path.add(nodeID);
    }

    @Override
    public List<String> getPath() {
        return path;
    }
}
