package cmrra.entity;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import core.Coord;

/**
 * Created by rabbiddog on 18.08.17.
 */
public class WanderTask extends HostTask {

    public Polygon searchArea;

    public Coord onWayTo;

    public double duration;

    public WanderTask(TaskType type, String unqId, Polygon searchArea, double duration)
    {
        super(type, unqId);
        this.searchArea = searchArea;
        this.onWayTo = null;
        this.duration = duration;
    }
    @Override
    public void setDone() {
        this.taskState = HostTaskState.COMPLETED;
    }

    @Override
    public void abort() {
        this.taskState = HostTaskState.ABORTED;
    }

    @Override
    public void pause() {
        this.taskState = HostTaskState.PAUSED;
    }
}
