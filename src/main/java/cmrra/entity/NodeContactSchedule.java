package cmrra.entity;

import com.vividsolutions.jts.geom.Geometry;

/**
 * Created by rabbiddog on 15.08.17.
 */
public class NodeContactSchedule {
    public String nodeId;
    public double lastTaskCompletionAt;
    public double interval;
    public Geometry contactArea;

    public NodeContactSchedule(String nodeId, double interval, Geometry contactArea)
    {
        this.nodeId = nodeId;
        this.interval = interval;
        this.contactArea = contactArea;
        this.lastTaskCompletionAt =0;
    }
}
