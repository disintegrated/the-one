package cmrra.entity;

import core.Coord;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by rabbiddog on 15.08.17.
 */
public class FeatureSurveySchedule {
    public final String featureId;
    public final MapFeature feature;
    public double lastTaskCompletionAt;
    public double interval;
    public MovementPattern movementPattern;
    public double taskDuration; //only for RANDOM movement pattern

    public LinkedList<Coord> _path;

    public FeatureSurveySchedule(String featureId, MapFeature feature, MovementPattern mp)
    {
        this.featureId = featureId;
        this.feature = feature;
        this.movementPattern = mp;
        lastTaskCompletionAt = 0;
    }

    public LinkedList<Coord> getPath()
    {
        List<Coord> ls = _path.stream().map(c -> new Coord(c.getX(), c.getY())).collect(Collectors.toList());
        LinkedList<Coord> lnk = new LinkedList<>();
        lnk.addAll(ls);
        return lnk;
    }

    public void setPath(LinkedList<Coord> p){
        List<Coord> ls = p.stream().map(c -> new Coord(c.getX(), c.getY())).collect(Collectors.toList());
        _path = new LinkedList<>();
        _path.addAll(ls);
    }
}
