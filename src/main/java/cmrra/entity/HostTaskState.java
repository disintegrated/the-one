package cmrra.entity;

/**
 * Created by rabbiddog on 10.08.17.
 */
public enum HostTaskState {
    SCHEDULED,
    RUNNING,
    PAUSED,
    ABORTED,
    COMPLETED,
    CREATED
}
