package cmrra.entity;

import core.Coord;

/**
 * Created by rabbiddog on 15.08.17.
 */
public class GoHomeTask extends HostTask {

    public Coord home;

    public GoHomeTask(Coord loc, String unqId)
    {
        super(TaskType.RECHARGE_BATTERY, unqId);
        home = loc;

    }
    @Override
    public void setDone() {
        taskState = HostTaskState.COMPLETED;
    }

    @Override
    public void abort() {
        taskState = HostTaskState.ABORTED;
    }

    @Override
    public void pause() {
        taskState = HostTaskState.PAUSED;
    }
}
