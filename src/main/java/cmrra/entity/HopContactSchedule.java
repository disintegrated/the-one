package cmrra.entity;

import cmrra.entity.config.DoubleTuple;
import com.vividsolutions.jts.geom.Geometry;

import java.util.LinkedList;

/**
 * Created by rabbiddog on 15.08.17.
 */
public class HopContactSchedule {
    public String nodeId;
    public double lastContactAt;
    public double lastContactAttemptAt;
    public DoubleTuple interval;
    public Geometry contactArea; //TODO: switch ths for a listof areas where the hop can be contacted

    public HopContactSchedule(String nodeId, DoubleTuple interval, Geometry contactArea)
    {
        this.nodeId = nodeId;
        this.interval = interval;
        this.contactArea = contactArea;
        this.lastContactAt = 0;
        this.lastContactAttemptAt = 0;
    }
}
