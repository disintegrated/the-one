package cmrra.entity;

/**
 * Created by rabbiddog on 09.08.17.
 */
public enum HostOperationState {
    SURVEY_REGION,
    RECHARGE_BATTERY,
    CONTACT_NODE,
    GO_HOME,
    WAIT_ASSIGNMENT,
    WANDER
}
