package cmrra.entity;

import cmrra.utility.geometry.GeometryUtil;
import com.vividsolutions.jts.geom.*;
import core.Coord;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by rabbiddog on 25.07.17.
 */
public class MapFeature {

    public final String featureId;
    public MapFeatureType featureType;
    public String sourceMapFile;
    public final Geometry feature;
    //remove this array list
    private final ArrayList<Coord> _coords;

    public MapFeature(String featureId, Geometry feature, ArrayList<Coord> coords)
    {
        this.featureId = featureId;
        this.feature = feature;
        this._coords = coords;
    }

    public MapFeature(String featureId, Geometry feature, ArrayList<Coord> coords, String sourceMap)
    {
        this.featureId = featureId;
        this.feature = feature;
        this._coords = coords;
        this.sourceMapFile = sourceMap;
    }

    public ArrayList<Coord> coords(){
        List<Coord> c = Arrays.stream(feature.getCoordinates()).map(GeometryUtil.funcCoordinateToCoord).collect(Collectors.toList());
        ArrayList<Coord> ac = new ArrayList<>();
        ac.addAll(c);
        return ac;
    }

    public void setFeatureType(MapFeatureType type)
    {
        this.featureType = type;
    }
}
