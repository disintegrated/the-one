package cmrra.entity.config;

/**
 * Created by rabbiddog on 20.07.17.
 */
import cmrra.movement.CMRRAMovement;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
public class Configuration {
    @XmlElement(name = "configurationID")
    public String configurationID;

    @XmlElement(name = "comment")
    public String comment;

    @XmlElement(name = "Group")
    public ArrayList<Group> hostGroups;

    @XmlElement(name="MapConfig")
    public MapConfig mapConfig;

    @XmlElement(name="MovementConfig")
    public MovementConfig movementConfig;

    @XmlElement(name = "ComInterfaces")
    public ArrayList<ComInterface> comInterfaces;

    @XmlElement(name = "mapLayouts")
    public ArrayList<MapLayout> mapLayouts;

    /**/
    @XmlElement(name = "mapCoordScale")
    public int mapCoordScale;

    @XmlElement(name = "ScenarioConfig")
    public ScenarioConfig scenarioConfig;

    @XmlElement(name = "ConnectionListeners")
    public ArrayList<String> connectionListeners;

    @XmlElement(name = "appConfigurations")
    public ArrayList<AppConfiguration> appConfigurations;

    @XmlElement(name = "forceDataDump")
    public  boolean forceDataDump;

    @XmlElement(name = "prioritizeData")
    public  boolean prioritizeData;
}
