package cmrra.entity.config;

/**
 * Created by rabbiddog on 20.07.17.
 */
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;

@XmlType(propOrder={"groupId","nrofHosts","movementModel","waitTime","speed"
        , "bufferSize", "router", "activeTimes", "activePeriod", "activePeriodOffset"
        , "msgTtl", "routeFile", "routeType", "comInterfaces", "nodeFeatureConstraints"
        , "surveyedRegion", "surveyPattern", "surveyInterval", "surveyPathDensity"
        , "groundNodeGroups", "groundNodeGroupsContactInterval", "groundNodeActivityDuration"
        , "hopNodeGroups", "hopContactInterval", "bootstrapGroups", "subscriptionType"
        , "growSubscription", "registeredApp"})
public class Group {

    // groupID : Group's identifier. Used as the prefix of host names
    @Size(max = 2)
    @XmlElement
    public String groupId;

    // nrofHosts: number of hosts in the group
    @XmlElement
    public int nrofHosts;

    // movementModel: movement model of the hosts (valid class name from movement package)
    @XmlElement
    public String movementModel;
    // waitTime: minimum and maximum wait times (seconds) after reaching destination
    @XmlElement
    public DoubleTuple waitTime;
    // speed: minimum and maximum speeds (m/s) when moving on a path
    @XmlElement
    public FloatTuple speed;
// bufferSize: size of the message buffer (bytes)
    @XmlElement
    public String bufferSize;
// router: router used to route messages (valid class name from routing package)
    @XmlElement
    public String router;
// activeTimes: Time intervals when the nodes in the group are active (start1, end1, start2, end2, ...)
    @XmlElement
    public ArrayList<IntTuple> activeTimes;

    //activePeriod
    @XmlElement
    public IntTuple activePeriod;

    @XmlElement
    public int activePeriodOffset;

// msgTtl : TTL (minutes) of the messages created by this host group, default=infinite
    @XmlElement
    public int msgTtl;
//// Group and movement model specific settings
// pois: Points Of Interest indexes and probabilities (poiIndex1, poiProb1, poiIndex2, poiProb2, ... )
//       for ShortestPathMapBasedMovement
// okMaps : which map nodes are OK for the group (map file indexes), default=all
//          for all MapBasedMovent models
// routeFile: route's file path - for MapRouteMovement
    @XmlElement
    public String routeFile;
            // routeType: route's type - for MapRouteMovement
    @XmlElement
    public int routeType;

    @XmlElement(name = "comInterface")
    public ArrayList<String> comInterfaces;

    @XmlElement
    public NodeFeatureConstraints nodeFeatureConstraints;

    //the region that this node can travel through and survey.
    //the can be found in the MapLayout collection of the Configuration class.
    //the geometry and location are stored in the Wkt map files.
    @XmlElement(name = "surveyedRegion")
    public String surveyedRegion;

    //SWEEP = 1001
    //RANDOM = 1002
    @XmlElement(name = "surveyPattern")
    public int surveyPattern;

    //interval in minutes between starting a new survey for each region
    //for ground nodes this is the interval between each wandering session
    @XmlElement(name = "surveyInterval")
    public long surveyInterval;

    @XmlElement(name = "surveyPathDensity")
    public long surveyPathDensity;

    //group of nodes that this node should actively try to establish contact with
    //these will denote the ground nodes that rely on the comm infrastructure provided by this node
    @XmlElement(name = "groundNodeGroups")
    public ArrayList<String> groundNodeGroups;

    //value for the drones to decide if a ground node should be contacted explicitly
    @XmlElement(name = "groundNodeGroupsContactInterval")
    public long groundNodeGroupsContactInterval;

    @XmlElement(name = "groundNodeActivityDuration")
    public long groundNodeActivityDuration;

    //other drones that act as hops
    @XmlElement(name= "hopNodeGroups")
    public ArrayList<String> hopNodeGroups;

    //ideally the interval should be lowered as much as possible
    //but avoid actively trying to make contact again if contacted with in this time frame
    @XmlElement(name = "hopContactInterval")
    public DoubleTuple hopContactInterval;

    //the nodes in this group might be aware of the locations of some group of nodes already
    //use this parameter to specify which groups are already known
    @XmlElement(name = "bootstrapGroups")
    public ArrayList<String> bootstrapGroups;

    @XmlElement(name = "subscriptionType")
    public SubscriptionType subscriptionType;

    //if true then the nodes assimilate the subscriptions of the neighbor
    //if false then the subscription is frozen
    @XmlElement(name = "growSubscription")
    public Boolean growSubscription;

    //app that will run on this group of nodes
    @XmlElement(name = "registeredApp")
    public ArrayList<String> registeredApp;
}
