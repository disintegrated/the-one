package cmrra.entity.config;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by rabbiddog on 21.07.17.
 */
@XmlType(propOrder = {"pos1","pos2"})
public class IntTuple {
    @XmlAttribute
    public int pos1;
    @XmlAttribute
    public int pos2;

    public IntTuple(){}
    public IntTuple(int pos1, int pos2)
    {
        this.pos1 = pos1;
        this.pos2 = pos2;
    }
}
