package cmrra.entity.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;

/**
 * Created by rabbiddog on 25.07.17.
 */

/**
 * This configuration class stores the information about the regions that are surveyed ,
 * if the feature is a region,
 * The feature ids of the remote stations in the region (only if isRegion Is true),
 * and the feature if of the base station only if it is in this region*/
@XmlType(propOrder = {"featureId", "isRegion", "stationIds", "baseStationId"})
public class MapLayout {

    public String featureId;

    public boolean isRegion;

    @XmlElement(name = "stationIds")
    public ArrayList<String> stationIds;

    public String baseStationId;
}
