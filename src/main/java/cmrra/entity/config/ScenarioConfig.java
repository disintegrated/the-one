package cmrra.entity.config;

import javax.xml.bind.annotation.XmlType;

/**
 * Created by rabbiddog on 07.08.17.
 */
@XmlType(propOrder={"name", "simulateConnections", "updateInterval", "endTime"})
public class ScenarioConfig {
    public String name;
    public boolean simulateConnections;
    public double updateInterval;
    // 43200s == 12h
    public int endTime;
}
