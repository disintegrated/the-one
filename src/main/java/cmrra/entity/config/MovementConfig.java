package cmrra.entity.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by rabbiddog on 22.07.17.
 */
@XmlType(propOrder = {"rngSeed","worldSize","warmup"})
public class MovementConfig {
    // seed for movement models' pseudo random number generator (default = 0)
    @XmlElement(name="RngSeed")
    public int rngSeed;
            // World's size for Movement Models without implicit size (width, height; meters)
    @XmlElement(name="WorldSize")
    public IntTuple worldSize;
            // How long time to move hosts in the world before real simulation
    @XmlElement(name="Warmup")
    public int warmup;
}
