package cmrra.entity.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by rabbiddog on 21.11.17.
 */
@XmlType(propOrder = {"paramValue"})
public class ConstructorParameter {

    @XmlElement(name = "paramValue")
    public Object paramValue;
}
