package cmrra.entity.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by rabbiddog on 20.11.17.
 */
@XmlType(propOrder = {"type", "t_start", "t_end", "v_start", "v_end"})
public class SubscriptionType {
    @XmlElement
    public byte type; //1=Sp2BiTemporalDynamicSubscription, 2=SpatialBiTemporalStaticSubscripton
    @XmlElement
    public long t_start; //in case of dynamic subscription this is the range to go back in time
    @XmlElement
    public long t_end; //ignore in case of dynamic subscription
    @XmlElement
    public long v_start;//in case of dynamic subscription this is the range to go back in time
    @XmlElement
    public long v_end;//ignore in case of dynamic subscription
/*
    public
    public SubscriptionType(long t_start, long t_end, long v_start, long v_end)
    {
        assert t_start <= t_end : "SubscriptionType: start transaction time should be less than equal to end time";
        assert v_start <= v_end : "SubscriptionType: start valid time should be less than equal to end time";

        this.type = 2;
        this.t_start = t_start;
        this.t_end = t_end;
        this.v_start = v_start;
        this.v_end = v_end;
    }

    public SubscriptionType(long t_start, long v_start)
    {
        this.type = 1;
        this.t_start = t_start;
        this.t_end = 0;
        this.v_start = v_start;
        this.v_end = 0;
    }*/
}
