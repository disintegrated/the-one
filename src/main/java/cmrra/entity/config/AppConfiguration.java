package cmrra.entity.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;

/**
 * Created by rabbiddog on 21.11.17.
 */
@XmlType(propOrder = {"appName", "appID", "parameters"})
public class AppConfiguration {

    @XmlElement(name="appName")
    public String appName;

    @XmlElement(name="appID")
    public String appID;

    @XmlElement(name="parameters")
    public ArrayList<ConstructorParameter> parameters;
}
