package cmrra.entity.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by rabbiddog on 22.07.17.
 */
@XmlType(propOrder = {"interfaceId","type","transmitSpeed","transmitRange"})
public class ComInterface {

    @XmlElement
    public String interfaceId;

    @XmlElement
    public String type;

    @XmlElement
    public String transmitSpeed;

    @XmlElement
    public double transmitRange;// in meters
}
