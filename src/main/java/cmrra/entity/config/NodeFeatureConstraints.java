package cmrra.entity.config;

/**
 * Created by rabbiddog on 25.07.17.
 */

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"batteryLife", "rechargeTime", "isDrone"})
public class NodeFeatureConstraints {

    //in minutes
    @XmlElement(name = "batteryLife")
    public long batteryLife;

    //in minutes
    @XmlElement(name = "rechargeTime")
    public long rechargeTime;

    @XmlElement
    public boolean isDrone;
}
