package cmrra.entity.config;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by rabbiddog on 21.07.17.
 */
@XmlType(propOrder = {"pos1","pos2"})
public class FloatTuple {
    @XmlAttribute
    public float pos1;
    @XmlAttribute
    public float pos2;

    public FloatTuple(){}
    public FloatTuple(float pos1, float pos2)
    {
        this.pos1 = pos1;
        this.pos2 = pos2;
    }
}
