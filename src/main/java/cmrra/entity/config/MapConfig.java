package cmrra.entity.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;

/**
 * Created by rabbiddog on 21.07.17.
 */
@XmlType(propOrder = {"noOfMaps", "mapFileNames"})
public class MapConfig {

    @XmlElement(name="NoOfMaps")
    public int noOfMaps;

    /**
     * The mapFileName is a relative path+ mapFileName in the application directory structure*/
    @XmlElement(name="MapFileNames")
    public ArrayList<String> mapFileNames;
}
