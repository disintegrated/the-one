package cmrra.entity.config;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by rabbiddog on 22.07.17.
 */
@XmlType(propOrder = {"pos1", "pos2"})
public class DoubleTuple {

    @XmlAttribute
    public double pos1;


    @XmlAttribute
    public double pos2;

    public DoubleTuple(){}

    public DoubleTuple(double pos1, double pos2)
    {
        this.pos1 = pos1;
        this.pos2 = pos2;
    }
}
