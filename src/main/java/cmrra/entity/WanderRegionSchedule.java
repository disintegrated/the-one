package cmrra.entity;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Polygon;

/**
 * Created by rabbiddog on 18.08.17.
 */
public class WanderRegionSchedule {
    public double lastTaskCompletionAt;
    public double restDuration;
    public double wanderDuration;
    public MapFeature wanderFeature;

    public WanderRegionSchedule(MapFeature wanderFeature, double restDuration, double wanderDuration)
    {
        this.restDuration = restDuration;
        this.wanderDuration = wanderDuration;
        this.wanderFeature = wanderFeature;
        this.lastTaskCompletionAt =0;
    }
}
