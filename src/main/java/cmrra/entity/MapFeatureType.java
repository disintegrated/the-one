package cmrra.entity;

/**
 * Created by rabbiddog on 25.07.17.
 */
public enum MapFeatureType {

    REGION_BOUNDARY,
    POINT_LOCATION
}
