package cmrra.entity;

/**
 * Created by rabbiddog on 07.09.17.
 */
public enum MovementPattern {

    SWEEP(1001),
    RANDOM(1002);
    private int value;
    private MovementPattern(int v)
    {
        this.value = v;
    }
    public int getValue() {
        return value;
    }

    public static MovementPattern fromInteger(int x) {
        switch(x) {
            case 1001:
                return MovementPattern.SWEEP;
            case 1002:
                return MovementPattern.RANDOM;
            default:
                return MovementPattern.SWEEP;
        }
    }
}
