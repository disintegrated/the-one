package cmrra.configuration;

/**
 * Created by rabbiddog on 20.07.17.
 */
import cmrra.entity.config.*;

import java.io.File;
import java.util.ArrayList;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class XMLConfigurationReaderWriter {

    private static XMLConfigurationReaderWriter _instance;
    private final String configurationFile = "default_configuration.xml";
    public final Configuration configuration;

    private XMLConfigurationReaderWriter(){
        configuration = loadConfiguration();
    }

    public static XMLConfigurationReaderWriter Instance()
    {
        if(null == _instance)
        {
            _instance = new XMLConfigurationReaderWriter();
        }
        return _instance;
    }

    private Configuration loadConfiguration() {
        /**confirm that the file exists*/
        File file = new File(configurationFile);
        if(!file.exists() || !file.isFile())
        {
        System.err.println("Unable to resolve settings file path");
            System.exit(0);
        }
        /**validate schema*/

        /**load into POJO*/
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Configuration.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Configuration _configuration = (Configuration) jaxbUnmarshaller.unmarshal(file);
            return _configuration;
        } catch (JAXBException e) {
            System.err.println(XMLConfigurationReaderWriter.class.getName()+": error occured while reading configuration file. "+e.getMessage());
            return null;
        }

    }

    public void exportConfigurationTemplate(String path)
    {
        File file = new File(path+System.getProperty("file.separator")+"default_configuration.xml");
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Configuration.class);

            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            Configuration configuration = getEmptyConfigurationObject();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(configuration, file);

        } catch (JAXBException e) {
            System.err.println(XMLConfigurationReaderWriter.class.getName()+": error occured while reading configuration file. "+e.getMessage());

        }
    }

    private Configuration getEmptyConfigurationObject()
    {
        Configuration config = new Configuration();
        config.hostGroups = new ArrayList<Group>();

        config.hostGroups.add(getEmptyGroupObject("g1"));
        config.hostGroups.add(getEmptyGroupObject("g2"));

        config.mapConfig = getEmptyMapConfigObject();

        config.movementConfig = getEmptyMovementConfigObject();

        config.comInterfaces = new ArrayList<ComInterface>();
        config.comInterfaces.add(getEmptyComInterfaceObject());

        config.mapLayouts = getEmptyMapLayoutObject();

        config.mapCoordScale = 1;

        config.scenarioConfig = getEmptyScenarioConfig();

        config.connectionListeners = getEmptyConnectionListeners();

        config.appConfigurations = new ArrayList<>();
        config.appConfigurations.add(getEmptyAppConfiguration());

        config.forceDataDump = false;
        config.prioritizeData = true;

        return  config;
    }

    private Group getEmptyGroupObject(String id)
    {
        Group g1 = new Group();
        g1.groupId = id;
        g1.nrofHosts=1;
        g1.movementModel="CMRRAMovement";
        g1.speed = new FloatTuple(1.2f, 2.0f);
        g1.waitTime = new DoubleTuple(10,20);
        g1.bufferSize = "50M";
        g1.router = "EpidemicRouter";
        g1.msgTtl = 300;
        g1.routeFile = "data/tram3.wkt";
        g1.routeType = 1;
        g1.comInterfaces = new ArrayList<String>();
        g1.comInterfaces.add("btInterface");
        g1.surveyedRegion = "region1";
        g1.surveyInterval = 1440; //min. equals a day
        g1.nodeFeatureConstraints = new NodeFeatureConstraints();
        g1.nodeFeatureConstraints.isDrone = true;
        g1.nodeFeatureConstraints.batteryLife = 60; //min
        g1.nodeFeatureConstraints.rechargeTime = 30; //min
        g1.groundNodeGroups = new ArrayList<>();
        g1.groundNodeGroupsContactInterval = 86400; //min. once every day
        g1.hopNodeGroups = new ArrayList<>();
        g1.hopContactInterval = new DoubleTuple(1200, 43200);
        g1.subscriptionType = getEmptySubscription();
        g1.growSubscription = true;
        return g1;
    }

    private SubscriptionType getEmptySubscription()
    {
        SubscriptionType subscriptionType = new SubscriptionType();
        subscriptionType.type = 2;
        subscriptionType.t_start = 179308;
        subscriptionType.t_end = 204692;
        subscriptionType.v_start = 179000;
        subscriptionType.v_end = 200005;
        return subscriptionType;
    }

    private MapConfig getEmptyMapConfigObject()
    {
        MapConfig mapConfig = new MapConfig();
        mapConfig.noOfMaps = 2;
        mapConfig.mapFileNames= new ArrayList<String>();
        mapConfig.mapFileNames.add("mapfile1");
        mapConfig.mapFileNames.add("mapfile2");
        return mapConfig;
    }

    private  MovementConfig getEmptyMovementConfigObject()
    {
        MovementConfig movementConfig = new MovementConfig();
        movementConfig.rngSeed = 1;
        movementConfig.warmup = 1000;
        movementConfig.worldSize = new IntTuple(4500, 3400);

        return  movementConfig;
    }

    private ComInterface getEmptyComInterfaceObject()
    {
        ComInterface comInterface = new ComInterface();
        comInterface.interfaceId = "btInterface";
        comInterface.type = "SimpleBroadcastInterface";
        comInterface.transmitRange = 100; //meters
        comInterface.transmitSpeed = "250k";

        return comInterface;
    }

    private ArrayList<MapLayout> getEmptyMapLayoutObject()
    {
        ArrayList<MapLayout> mapLayouts = new ArrayList<>();

        MapLayout l1 = new MapLayout();
        l1.featureId = "region1";
        l1.isRegion = true;
        l1.baseStationId = "rmtStn11";
        l1.stationIds = new ArrayList<>();
        l1.stationIds.add("rmtStn11");

        MapLayout l2 = new MapLayout();
        l2.featureId = "region2";
        l2.isRegion = true;
        l2.baseStationId = null;
        l2.stationIds = new ArrayList<>();
        l2.stationIds.add("rmtStn21");

        mapLayouts.add(l1);
        mapLayouts.add(l2);

        return mapLayouts;
    }

    private ScenarioConfig getEmptyScenarioConfig()
    {
        ScenarioConfig c = new ScenarioConfig();
        c.endTime = 43200*2; //43200s = 12h
        c.name = "CMRRA";
        c.simulateConnections = false;
        c.updateInterval = 0.1;

        return c;
    }

    private ArrayList<String> getEmptyConnectionListeners()
    {
        ArrayList<String> res = new ArrayList<>();
        res.add("ConnectivityONEReport");
        return res;
    }

    private AppConfiguration getEmptyAppConfiguration()
    {
        AppConfiguration appConfiguration = new AppConfiguration();
        appConfiguration.appName = "TemperatureSensor";
        appConfiguration.appID = "TEMPSENS10936";
        appConfiguration.parameters = new ArrayList<>();
        ConstructorParameter p1 = new ConstructorParameter();
        p1.paramValue = 1200;
        appConfiguration.parameters.add(p1);
        return appConfiguration;
    }
}
