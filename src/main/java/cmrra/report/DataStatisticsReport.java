package cmrra.report;

import cmrra.configuration.XMLConfigurationReaderWriter;
import cmrra.data.index.entity.Spatial2DBiTemporalData;
import cmrra.entity.CachedMapFeatureStore;
import cmrra.entity.config.Group;
import cmrra.entity.data.Command;
import cmrra.entity.data.ONData;
import cmrra.entity.data.TemperatureData;
import core.ConnectionListener;
import core.DTNHost;
import io.reactivex.observers.DisposableObserver;
import org.apache.commons.lang3.tuple.Pair;
import report.Report;

import java.util.*;
import java.util.stream.DoubleStream;

public class DataStatisticsReport extends Report implements ConnectionListener {

    private HashMap<String, DTNHost> allHosts;

    public DataStatisticsReport(){init();}

    @Override
    public void init() {
        super.init();
        allHosts = new HashMap<>();
    }

    @Override
    public void hostsConnected(DTNHost host1, DTNHost host2) {

        if(!allHosts.containsKey(host1.getName()))
            allHosts.put(host1.getName(), host1);

        if(!allHosts.containsKey(host2.getName()))
            allHosts.put(host2.getName(), host2);
    }

    @Override
    public void hostsDisconnected(DTNHost host1, DTNHost host2) {

    }

    @Override
    public void done()
    {
        write("Configuration: "+ XMLConfigurationReaderWriter.Instance().configuration.configurationID);
        write(XMLConfigurationReaderWriter.Instance().configuration.comment);
        write("\n");
        write("\n");
        write("Simulation Duration: "+ XMLConfigurationReaderWriter.Instance().configuration.scenarioConfig.endTime);
        //for all hosts note down the data stats

        //HashMap<Integer, List<Double>> hopsToTime = new HashMap<>();
        HashMap<String, HashSet<DTNHost>> regionGroup = groupByRegion();
        for (Map.Entry<String, HashSet<DTNHost>> e: regionGroup.entrySet())
        {
            write("Region:"+e.getKey());
            for (DTNHost host: e.getValue())
            {
                write("Host:"+host.getName() + " store name: " + host.store().getName());
                int[] count = new int[1];
                int[] hop = new int[1];
                double[] time = new double[1];
                Spatial2DBiTemporalData<String>[] earliest_and_latest_data = new Spatial2DBiTemporalData[2];

                io.reactivex.Observable<Spatial2DBiTemporalData<String>> observable = host.store().streamAll();
                DisposableObserver<Spatial2DBiTemporalData<String>> observer = new DisposableObserver<Spatial2DBiTemporalData<String>>() {
                    @Override
                    public void onNext(Spatial2DBiTemporalData<String> data) {

                        if(data instanceof ONData)
                        {
                            ONData d = (ONData)data;
                            hop[0] = hop[0] + d.getPath().size();
                        }
                        if(null != earliest_and_latest_data[0])
                        {
                            if(earliest_and_latest_data[0].BB().V().Start() > data.BB().V().Start())
                                earliest_and_latest_data[0] = data;
                        }else {
                            earliest_and_latest_data[0] = data;
                        }

                        if(null != earliest_and_latest_data[1])
                        {
                            if(earliest_and_latest_data[1].BB().V().Start() < data.BB().V().Start())
                                earliest_and_latest_data[1] = data;
                        }else {
                            earliest_and_latest_data[1] = data;
                        }
                        double latency = data.BB().T().entryAt() - data.BB().V().Start();
                        time[0] = time[0]+latency;
                        count[0]= count[0]+1;
                    }

                    @Override
                    public void onError(Throwable e) {
                        cmrra.data.index.utility.Log.error(cmrra.data.index.utility.Log.getExceptionString(e));
                    }

                    @Override
                    public void onComplete() {
                        cmrra.data.index.utility.Log.info("DataStatistics for host: "+host.getName()+" done");
                    }
                };

                observable.subscribe(observer);
                observer.dispose();

                write("DataSize: "+host.getName() + " :"+count[0]);
                write("Average number of Hops: "+ (count[0] == 0 ?0: hop[0]/count[0]));
                write("Average latency: "+ (count[0] == 0? 0: time[0]/count[0]));
                if(null != earliest_and_latest_data[0])
                    write("Earliest data from :"+earliest_and_latest_data[0].BB().V().Start()+"  reached at :"+earliest_and_latest_data[0].BB().T().entryAt()+" . Latency : "+(earliest_and_latest_data[0].BB().T().entryAt() - earliest_and_latest_data[0].BB().V().Start()));
                if(null != earliest_and_latest_data[1])
                    write("Latest data from :"+earliest_and_latest_data[1].BB().V().Start()+"  reached at :"+earliest_and_latest_data[1].BB().T().entryAt()+" . Latency : "+(earliest_and_latest_data[1].BB().T().entryAt() - earliest_and_latest_data[1].BB().V().Start()));
                write("\n");
                write("\n");
            }
        }

        super.done();
    }

    private HashMap<String, HashSet<DTNHost>> groupByRegion()
    {
        HashMap<String, HashSet<DTNHost>> group = new HashMap<>();

        for (Map.Entry<String, DTNHost> e:allHosts.entrySet())
        {
            HashSet<DTNHost> set;
            String region = XMLConfigurationReaderWriter.Instance().configuration.hostGroups
                .stream().filter(s -> s.groupId == e.getValue().getGroup()).findFirst().get().surveyedRegion;

            if(group.containsKey(region))
            {
                set = group.get(region);

            }
            else
            {
                set = new HashSet<>();
            }
            set.add(e.getValue());
            group.put(region, set);
        }

        return group;
    }

}
