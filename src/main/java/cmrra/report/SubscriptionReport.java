package cmrra.report;

import cmrra.configuration.XMLConfigurationReaderWriter;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import cmrra.entity.CachedMapFeatureStore;
import cmrra.entity.MapFeature;
import cmrra.entity.config.MapLayout;
import cmrra.routing.Subscription;
import cmrra.utility.geometry.GeometryUtil;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Polygon;
import core.ConnectionListener;
import core.DTNHost;
import core.SimClock;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import report.Report;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.*;

public class SubscriptionReport extends Report implements ConnectionListener {

    private HashMap<String, DTNHost> allHosts;
    private HashMap<String, List<Pair<Double,Double>>> percentageOfRegion;
    private double totalArea;
    public SubscriptionReport()
    {
        init();
    }

    @Override
    public void init() {
        super.init();
        allHosts = new HashMap<>();
        percentageOfRegion = new HashMap<>();
        totalArea = 0.0;
    }
    @Override
    public void hostsConnected(DTNHost host1, DTNHost host2) {
        if(totalArea == 0.0)
        {
            double xmin= Double.MAX_VALUE;
            double xmax=0.0;
            double ymin=Double.MAX_VALUE;
            double ymax = 0.0;
            List<MapLayout> features = XMLConfigurationReaderWriter.Instance()
                    .configuration.mapLayouts.stream().filter(m -> m.isRegion).collect(Collectors.toList());

            for (MapLayout layout: features)
            {
                Envelope env = CachedMapFeatureStore.Instance().getFeature(layout.featureId).feature.getEnvelopeInternal();
                xmin = xmin > env.getMinX()? env.getMinX():xmin;
                xmax = xmax< env.getMaxX() ? env.getMaxX() : xmax;
                ymin = ymin > env.getMinY() ? env.getMinY() : ymin;
                ymax = ymax < env.getMaxY() ? env.getMaxY() : ymax;
            }

            Coordinate[] seq = new Coordinate[5];
            seq[0] = new Coordinate(xmin, ymin);
            seq[1] = new Coordinate(xmax, ymin);
            seq[2] = new Coordinate(xmax, ymax);
            seq[3] = new Coordinate(xmin, ymax);
            seq[4] = new Coordinate(xmin, ymin);
            Polygon p = GeometryUtil.GEOMETRY_FACTORY.createPolygon(seq);

            totalArea = p.getArea();
            write("Total Area" + totalArea);
            write("Region");
            write(p.toText());
        }

        if(!allHosts.containsKey(host1.getName()))
            allHosts.put(host1.getName(), host1);
        if(!percentageOfRegion.containsKey(host1.getName()))
        {
            List<Pair<Double, Double>> p = new LinkedList<>();
            percentageOfRegion.put(host1.getName(), p);
        }
        if(!allHosts.containsKey(host2.getName()))
            allHosts.put(host2.getName(), host2);

        if(!percentageOfRegion.containsKey(host2.getName()))
        {
            List<Pair<Double, Double>> p = new LinkedList<>();
            percentageOfRegion.put(host2.getName(), p);
        }
    }

    @Override
    public void hostsDisconnected(DTNHost host1, DTNHost host2)
    {

        Sp2DBiTemporalBoundary boundary1 = host1.getOverArchSubscription().getSubscritpionBoundary();
        Coordinate[] seq1 = new Coordinate[5];
        seq1[0] = new Coordinate(boundary1.X().start, boundary1.Y().start);
        seq1[1] = new Coordinate(boundary1.X().end, boundary1.Y().start);
        seq1[2] = new Coordinate(boundary1.X().end, boundary1.Y().end);
        seq1[3] = new Coordinate(boundary1.X().start, boundary1.Y().end);
        seq1[4] = new Coordinate(boundary1.X().start, boundary1.Y().start);
        Polygon p1 = GeometryUtil.GEOMETRY_FACTORY.createPolygon(seq1);

        double a1 = p1.getArea();
        List<Pair<Double, Double>> pair1 = percentageOfRegion.get(host1.getName());
        pair1.add(new ImmutablePair<>(SimClock.getTime(), a1*100/totalArea));


        Sp2DBiTemporalBoundary boundary2 = host2.getOverArchSubscription().getSubscritpionBoundary();

        Coordinate[] seq2 = new Coordinate[5];
        seq2[0] = new Coordinate(boundary2.X().start, boundary2.Y().start);
        seq2[1] = new Coordinate(boundary2.X().end, boundary2.Y().start);
        seq2[2] = new Coordinate(boundary2.X().end, boundary2.Y().end);
        seq2[3] = new Coordinate(boundary2.X().start, boundary2.Y().end);
        seq2[4] = new Coordinate(boundary2.X().start, boundary2.Y().start);
        Polygon p2 = GeometryUtil.GEOMETRY_FACTORY.createPolygon(seq2);

        double a2 = p2.getArea();
        List<Pair<Double, Double>> pair2 = percentageOfRegion.get(host2.getName());
        pair2.add(new ImmutablePair<>(SimClock.getTime(), a2*100/totalArea));

    }

    @Override
    public void done()
    {
        write("Configuration: "+ XMLConfigurationReaderWriter.Instance().configuration.configurationID);
        write(XMLConfigurationReaderWriter.Instance().configuration.comment);
        write("\n");
        write("\n");
        write("Simulation Duration: "+ XMLConfigurationReaderWriter.Instance().configuration.scenarioConfig.endTime);
        //for all hosts note down the data stats
        for (DTNHost host: allHosts.values())
        {
            write("Host:"+host.getName());
            write("percentage of spatial area covered");
            write("Time \t\t %");
            List<Pair<Double, Double>> l = percentageOfRegion.get(host.getName());
            for (Pair<Double, Double> pair: l)
            {
                write(pair.getLeft()+"\t\t"+pair.getRight());
            }
            write("Final subscription range");
            Sp2DBiTemporalBoundary boundary = host.getOverArchSubscription().getSubscritpionBoundary();
            write("Spatial boundary");
            Coordinate[] seq = new Coordinate[5];
            seq[0] = new Coordinate(boundary.X().start, boundary.Y().start);
            seq[1] = new Coordinate(boundary.X().end, boundary.Y().start);
            seq[2] = new Coordinate(boundary.X().end, boundary.Y().end);
            seq[3] = new Coordinate(boundary.X().start, boundary.Y().end);
            seq[4] = new Coordinate(boundary.X().start, boundary.Y().start);
            Polygon p = GeometryUtil.GEOMETRY_FACTORY.createPolygon(seq);
            write(p.toText());
            write("Transaction Time");
            write(boundary.T().toString());
            write("\n");
            write("Valid Time");
            write(boundary.V().toString());
            write("\n");
            write("\n");
        }

        super.done();
    }
}
