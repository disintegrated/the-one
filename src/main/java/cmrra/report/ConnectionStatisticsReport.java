package cmrra.report;

import cmrra.configuration.XMLConfigurationReaderWriter;
import cmrra.entity.config.Group;
import core.*;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import report.Report;
import cmrra.data.index.utility.Log;

import java.util.*;

public class ConnectionStatisticsReport extends Report implements ConnectionListener {

    List<ConnStat> allConnectionStats;
    HashMap<String, String> hostRegion; //<Hostname, regionid>

    public ConnectionStatisticsReport()
    {
        init();
    }

    @Override
    public void init() {
        super.init();
        allConnectionStats = new LinkedList<>();
        hostRegion = new HashMap<>();
    }

    @Override
    public void hostsConnected(DTNHost host1, DTNHost host2)
    {
        try{
            if(!hostRegion.containsKey(host1.getName()))
                hostRegion.put(host1.getName(), getRegionOfOperation(host1));
            if(!hostRegion.containsKey(host2.getName()))
                hostRegion.put(host2.getName(), getRegionOfOperation(host2));
            ConnStat stat = getRelevantConnStat( host1,  host2);

            if(null == stat)
            {
                Connection thisConn = host1.getConnections().stream()
                        .filter(c -> {
                            DTNHost other = c.getOtherNode(host1);
                            if(other == host2)
                                return true;
                            else
                                return false;
                        }).findFirst().get();
                stat = new ConnStat(host1, host2, thisConn);
                allConnectionStats.add(stat);
            }

            stat.hostsConnected();
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionMessage(ex));
        }
    }

    @Override
    public void hostsDisconnected(DTNHost host1, DTNHost host2) {
        try {
            ConnStat stat = getRelevantConnStat( host1,  host2);

            if(null == stat)
                throw new SimError(this.getClass().getCanonicalName()+" threw error on hostsDisconnected()" +
                        ". Relevant connection statistics for hosts "+host1.getName()+ " and "+host2.getName() +" not found");

            stat.hostsDisconnected();
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionMessage(ex));
        }
    }

    private class ConnStat{
        public DTNHost host1, host2;
        Connection conn;
        int bytesTransferred;
        double time;
        List<Triple<Double, Double, Integer>> bytesPerContact;

        public ConnStat(DTNHost h1, DTNHost h2, Connection c)
        {
            host1 = h1;
            host2 = h2;
            conn = c;
            bytesTransferred=0;
            time = 0.0;
            bytesPerContact = new LinkedList<>();
        }

        public void hostsConnected() throws Exception
        {
            bytesTransferred = conn.getTotalDataBytesTransferred();
            time = SimClock.getTime();

        }

        public void hostsDisconnected() throws Exception
        {
            bytesTransferred = conn.getTotalDataBytesTransferred() - bytesTransferred;
            time = SimClock.getTime() - time;
            bytesPerContact.add(new ImmutableTriple<>(SimClock.getTime(), time, bytesTransferred));
        }
    }

    private ConnStat getRelevantConnStat(DTNHost host1, DTNHost host2)
    {
        ConnStat stat = allConnectionStats.stream()
                .filter(s -> Objects.nonNull(s))
                .filter(s -> {
                    if((s.host1 == host1 && s.host2 == host2) || (s.host1 == host2 && s.host2 == host1))
                        return true;
                    else
                        return false;
                }).findFirst().orElse(null);

        return stat;
    }

    @Override
    public void done()
    {
        write("Configuration: "+ XMLConfigurationReaderWriter.Instance().configuration.configurationID);
        write(XMLConfigurationReaderWriter.Instance().configuration.comment);
        write("\n");
        write("\n");
        write("Simulation Duration: "+ XMLConfigurationReaderWriter.Instance().configuration.scenarioConfig.endTime);
        for (ConnStat stat: allConnectionStats)
        {
            write("Connection between Host "+stat.host1.getName()+" and Host "+stat.host2.getName());
            write("At \t\t Duration \t\t BytesTransfered");
            for (Triple<Double, Double, Integer> p:stat.bytesPerContact)
            {
                write(p.getLeft() + " \t\t " + p.getMiddle()+ " \t\t "+ p.getRight());
            }

            write("No. of Contacts: "+ stat.bytesPerContact.size());
            write("Avg. per hr:" + stat.bytesPerContact.size() /(XMLConfigurationReaderWriter.Instance().configuration.scenarioConfig.endTime / 3600));
            write("\n");
            write("\n");
        }
        write("\n");
        HashMap<String, HashSet<String>> gr = groupByRegion();
        write("Region of operation: \t\t Hosts \n");
        for (Map.Entry<String, HashSet<String>> e:gr.entrySet())
        {
            write(e.getKey()+": \n ");
            for (String h:e.getValue())
            {
                write(h + ",");
            }
            write("\n");
        }

        super.done();
    }

    private String getRegionOfOperation(DTNHost h)
    {
        Group g = XMLConfigurationReaderWriter.Instance().configuration.hostGroups
                .stream().filter(s -> s.groupId == h.getGroup()).findFirst().get();

        return g.surveyedRegion;
    }

    private HashMap<String, HashSet<String>> groupByRegion()
    {
        HashMap<String, HashSet<String>> group = new HashMap<>();

        for (Map.Entry<String, String> e:hostRegion.entrySet())
        {
            HashSet<String> set;
            if(group.containsKey(e.getValue()))
            {
                set = group.get(e.getValue());

            }
            else
            {
                set = new HashSet<>();
            }
            set.add(e.getKey());
            group.put(e.getValue(), set);
        }

        return group;
    }
}
