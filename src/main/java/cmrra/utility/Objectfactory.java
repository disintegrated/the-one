package cmrra.utility;

import cmrra.entity.config.ConstructorParameter;
import core.SettingsError;
import core.SimError;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

/**
 * Created by rabbiddog on 22.07.17.
 */
public class Objectfactory {

    public static Object createIntializedObject(String className)
    {
        //TODO: resolve dependency and inject
        Object o;
        Class<?> objClass = getClass(className);

        try
        {
            o = objClass.newInstance();

        } catch (SecurityException e) {
            e.printStackTrace();
            throw new SettingsError("Fatal exception " + e, e);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            throw new SettingsError("Fatal exception " + e, e);
        }  catch (InstantiationException e) {
            throw new SettingsError("Can't create an instance of '" +
                    className + "'", e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new SettingsError("Fatal exception " + e, e);
        }

        return o;
    }

    public static Object createParameterizedObject(String className, ArrayList<ConstructorParameter> params)
    {
        //TODO: resolve dependency and inject
        Object o;
        Class<?> objClass = getClass(className);
        Object[] args = new Object[params.size()];
        Class[] argsClass = new Class[params.size()];

        try
        {
            int i= 0 ;
            for (ConstructorParameter p:params)
            {
                args[i] = p.paramValue;
                argsClass[i++] = p.paramValue.getClass();
            }
            Constructor<?> constructor = objClass.getConstructor(argsClass);
            o = constructor.newInstance(args);

        } catch (SecurityException e) {
            e.printStackTrace();
            throw new SettingsError("Fatal exception " + e, e);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            throw new SettingsError("Fatal exception " + e, e);
        }  catch (InstantiationException e) {
            throw new SettingsError("Can't create an instance of '" +
                    className + "'", e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new SettingsError("Fatal exception " + e, e);
        }
        /*catch (ClassNotFoundException e)
        {
            e.printStackTrace();
            throw new SettingsError("Fatal exception " + e, e);
        }*/
        catch (NoSuchMethodException e)
        {
            e.printStackTrace();
            throw new SettingsError("Fatal exception " + e, e);
        }
        catch (InvocationTargetException e)
        {
            e.printStackTrace();
            throw new SettingsError("Fatal exception " + e, e);
        }

        return o;
    }


    /**
     * Returns a Class object for the name of class of throws SettingsError
     * if such class wasn't found.
     * @param name Full name of the class (including package name)
     * @return A Class object of that class
     * @throws SettingsError if such class wasn't found or couldn't be loaded
     */
    private static Class<?> getClass(String name) {
        String className = name;
        Class<?> c;

        try {
            c = Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new SettingsError("Couldn't find class '" + className + "'"+
                    "\n" + e.getMessage(),e);
        }

        return c;
    }
}
