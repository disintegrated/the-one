package cmrra.utility.geometry;

import cmrra.configuration.XMLConfigurationReaderWriter;
import core.Coord;
import core.SimError;
import input.WKTReader;

import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.text.ParseException;
import java.util.Scanner;
import java.util.concurrent.*;
import org.javatuples.*;

import com.vividsolutions.jts.geom.*;
/**
 * Created by rabbiddog on 24.07.17.
 */
public class WktMapParser extends WKTReader {

    private static final String EMPTY = "EMPTY";
    private static final String COMMA = ",";
    private static final String L_PAREN = "(";
    private static final String R_PAREN = ")";
    private static final String EOF = "EOF";
    private static final String DELIM = "DELIM";
    private String currentMapFileName;
    private int mapCoordScale = 1;


    /**
     * Reads a Wkt file to return all wkt line related to Polygons .
     * The file can contain other features but they will be ignored.
     * Polygon, Multipolygons and Points are identified so far
     * @param mapFileName relative path to the map file
     * @param delimiter text to separate a feature id  from its wtk geometry
     * @return A map of the feature id and a tuple of the geature coordinates and the feature. The Geometry
     * contains the child object
     */
    public ConcurrentHashMap<String, Pair<ArrayList<Coord>, Geometry>> parseMapFeatures(String mapFileName, char delimiter)
    {
        if('\u0000' == delimiter)
        {
            delimiter = '|';
        }

        currentMapFileName = mapFileName;
        if(XMLConfigurationReaderWriter.Instance().configuration.mapCoordScale > 1)
            mapCoordScale = XMLConfigurationReaderWriter.Instance().configuration.mapCoordScale;

        ConcurrentHashMap<String, Pair<ArrayList<Coord>, Geometry>> featuresInMap = new ConcurrentHashMap<String, Pair<ArrayList<Coord>, Geometry>>() ;
        try{
            FileReader fr = new FileReader(mapFileName);
            init(fr);
            StreamTokenizer tokenizer = new StreamTokenizer(reader);
            setUpTokenizer(tokenizer);
            if('|' != delimiter)
            {
                addTokenizer(delimiter, tokenizer);
            }


            String previousTokenStr = null;
            String featureId = null;
            String type;
            String delStr = new String(new char[]{delimiter});
            while((type = getNextWord(tokenizer)) != EOF)
            {
                if(type.equals(delStr))
                {
                    if(null == previousTokenStr)
                    {
                        throw new SimError("Feature Id not found before geometry text in map file "+currentMapFileName);
                    }
                    featureId = previousTokenStr;
                }
                else if (type.equalsIgnoreCase(POLYGON))
                {
                    Pair<ArrayList<Coord>,Geometry> featureDetail= readPolygonText(tokenizer);
                    featuresInMap.put(featureId, featureDetail);
                    previousTokenStr = null;
                    featureId = null;
                }
                /*else if (type.equalsIgnoreCase(MULTIPOLYGON))
                {
                    return readMultiPolygonText(tokenizer);
                    polygonsInMap.put(featureId, poly);
                    previousTokenStr = null;
                    featureId = null;
                }*/
                else if(type.equalsIgnoreCase(POINT))
                {
                    Pair<ArrayList<Coord>,Geometry> featureDetail= readPointText(tokenizer);
                    featuresInMap.put(featureId, featureDetail);
                    previousTokenStr = null;
                    featureId = null;
                }
                else
                {
                    previousTokenStr = type;
                }
            }


        }catch (IOException ioex)
        {
            throw new SimError("Error while trying to read map file "+currentMapFileName, ioex);
        }
        catch (ParseException pex)
        {
            throw new SimError("Error while trying to read map file " + currentMapFileName, pex);
        }

        return featuresInMap;
    }


    /***
     * @param tokenizer the Stream tokenizer intitialized with the word char
     * @return A tuple of list of coordinates and the feature geometry. The geometry object is of type Polygon
     * @throws IOException
     * @throws ParseException
     */
    private Pair<ArrayList<Coord>,Geometry> readPolygonText(StreamTokenizer tokenizer) throws IOException, ParseException
    {
        String nextToken = getNextEmptyOrOpener(tokenizer);
        if (nextToken.equals(EMPTY))
        {
            return null;
        }

        ArrayList<Coord> coordList = new ArrayList<>();
        ArrayList<Coordinate> coordinates = new ArrayList<>();
        boolean pathStarted = false;
        int parOpen=1;
        Scanner tupleScan;
        String type;
        StringBuffer coordTuple = new StringBuffer("");
        while (parOpen > 0)
        {
            type = getNextWord(tokenizer);
            if(type.equals(L_PAREN))
            {
                parOpen++;
            }
            else if(type.equals(COMMA) || type.equals(R_PAREN))
            {
                if(coordTuple.length() > 0)
                {
                    tupleScan = new Scanner(coordTuple.toString());
                    double x = Double.parseDouble(tupleScan.next()) * mapCoordScale;
                    double y = Double.parseDouble(tupleScan.next()) * mapCoordScale;
                    coordList.add(new Coord(x,y));
                    coordTuple.setLength(0);
                    coordinates.add(new Coordinate(x,y));

                }

                if(type.equals(R_PAREN))
                {
                    parOpen--;
                }
            }
            else if(type.length() > 0){
                coordTuple.append(type);
                coordTuple.append(" ");
            }
        }

        Coordinate[] crdArr = new Coordinate[coordinates.size()];
        coordinates.toArray(crdArr);
        Polygon poly = GeometryUtil.GEOMETRY_FACTORY.createPolygon(crdArr);
        return new Pair<>(coordList, poly);
    }


    private Pair<ArrayList<Coord>,Geometry> readPointText(StreamTokenizer tokenizer) throws IOException, ParseException
    {

        String nextToken = getNextEmptyOrOpener(tokenizer);
        if (nextToken.equals(EMPTY))
        {
            return null;
        }

        int parOpen=1;
        String type;
        StringBuffer coordTuple = new StringBuffer("");
        Scanner tupleScan;
        ArrayList<Coord> l = new ArrayList<>();
        Point p = null;
        while (parOpen > 0)
        {
            type = getNextWord(tokenizer);
            if(type.equals(L_PAREN))
            {
                parseError("x:coord", tokenizer);
            }
            else if(type.equals(COMMA))
            {
                parseError("x:coord", tokenizer);
            }
            else if (type.equals(R_PAREN))
            {
                if(coordTuple.length() == 0)
                {
                    parseError("coord", tokenizer);
                }
                tupleScan = new Scanner(coordTuple.toString());
                double x = Double.parseDouble(tupleScan.next()) * mapCoordScale;
                double y = Double.parseDouble(tupleScan.next()) * mapCoordScale;
                l.add(new Coord(x,y));
                p = GeometryUtil.GEOMETRY_FACTORY.createPoint(new Coordinate(x,y));

                parOpen--;
            }
            else if(!type.equals(EMPTY))
            {
                coordTuple.append(type);
                coordTuple.append(" ");
            }
        }

        return new Pair<>(l, p);
    }


    //region geotools
    //source https://github.com/geotools/geotools/blob/master/modules/library/main/src/main/java/org/geotools/geometry/text/WKTParser.java

    /**
     * Sets up a {@link StreamTokenizer} for use in parsing the geometry text
     */
    private void setUpTokenizer(StreamTokenizer tokenizer) {
        final int char128 = 128;
        final int skip32 = 32;
        final int char255 = 255;
        // set tokenizer to NOT parse numbers
        tokenizer.resetSyntax();
        tokenizer.wordChars('a', 'z');
        tokenizer.wordChars('A', 'Z');
        tokenizer.wordChars(char128 + skip32, char255);
        tokenizer.wordChars('0', '9');
        tokenizer.wordChars('-', '-');
        tokenizer.wordChars('+', '+');
        tokenizer.wordChars('.', '.');
        tokenizer.wordChars('|', '|');
        tokenizer.whitespaceChars(0, ' ');
        tokenizer.commentChar('#');
    }

    private void addTokenizer(char c, StreamTokenizer tokenizer)
    {
        tokenizer.wordChars(c, c);
    }

    /**
     * Returns the next word in the stream.
     *
     * @return the next word in the stream as uppercase text
     * @throws ParseException if the next token is not a word
     * @throws IOException    if an I/O error occurs
     */
    private String getNextWord(StreamTokenizer tokenizer) throws IOException, ParseException {
        int type = tokenizer.nextToken();
        String value;
        switch (type) {
            case StreamTokenizer.TT_WORD:
                String word = tokenizer.sval;
                if (word.equalsIgnoreCase(EMPTY)) {
                    value = EMPTY;
                }
                value = word;
                break;
            case'(':
                value = L_PAREN;
                break;
            case')':
                value = R_PAREN;
                break;
            case',':
                value = COMMA;
                break;
            case StreamTokenizer.TT_EOF:
                    value = EOF;
                    break;
            default:
                parseError("word", tokenizer);
                value = null;
                break;
        }
        return value;
    }

    /**
     * Throws a formatted ParseException for the current token.
     *
     * @param expected a description of what was expected
     * @throws ParseException
     */
    private void parseError(String expected, StreamTokenizer tokenizer)
            throws ParseException {
        String tokenStr = tokenString(tokenizer);
        ParseException p = new ParseException("Expected " + expected + " but found " + tokenStr, 0);

        throw new SimError("Error while parsing wkt file "+currentMapFileName, p);
    }

    /**
     * Gets a description of the current token
     *
     * @return a description of the current token
     */
    private String tokenString(StreamTokenizer tokenizer) {
        switch (tokenizer.ttype) {
            case StreamTokenizer.TT_NUMBER:
                return "<NUMBER>";
            case StreamTokenizer.TT_EOL:
                return "End-of-Line";
            case StreamTokenizer.TT_EOF:
                return "End-of-Stream";
            case StreamTokenizer.TT_WORD:
                return "'" + tokenizer.sval + "'";
            default:
        }
        return "'" + (char) tokenizer.ttype + "'";
    }

    /**
     * Returns the next EMPTY or L_PAREN in the stream as uppercase text.
     *
     * @param tokenizer tokenizer over a stream of text in Well-known Text
     *                  format. The next token must be EMPTY or L_PAREN.
     * @return the next EMPTY or L_PAREN in the stream as uppercase
     *         text.
     * @throws ParseException if the next token is not EMPTY or L_PAREN
     * @throws IOException    if an I/O error occurs
     */
    private String getNextEmptyOrOpener(StreamTokenizer tokenizer) throws IOException, ParseException {
        String nextWord = getNextWord(tokenizer);
        if (nextWord.equals(EMPTY) || nextWord.equals(L_PAREN)) {
            return nextWord;
        }
        parseError(EMPTY + " or " + L_PAREN, tokenizer);
        return null;
    }

    //endregion geotools
}
