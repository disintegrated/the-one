package cmrra.utility.geometry;

import com.vividsolutions.jts.geom.util.AffineTransformation;
import com.vividsolutions.jts.operation.buffer.BufferOp;
import com.vividsolutions.jts.operation.distance.DistanceOp;
import core.Coord;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import com.vividsolutions.jts.geom.*;
import org.apache.commons.lang3.ArrayUtils;

/**
 * Created by rabbiddog on 26.07.17.
 */
public class GeometryUtil {

    public static final Random rnd= new Random();
    public static final GeometryFactory GEOMETRY_FACTORY = new GeometryFactory();
    public static final AffineTransformation AFFINE_TRANSFORMATION = new AffineTransformation();
    public static Random rng = new Random();

    public  static Function<Coord, Point> funcCordToPoint = c -> {
        return GEOMETRY_FACTORY.createPoint(new Coordinate(c.getX(),c.getY()));
    };

    public  static Function<Point, Coord> funcPointToCord = p ->{
        return new Coord(p.getX(), p.getY());
    };

    public static Function<Coord, Coordinate> funcCoordToCoordinate = c ->{
        return new Coordinate(c.getX(), c.getY());
    };

    public static Function<Coordinate, Coord> funcCoordinateToCoord = c ->{
      return new Coord(c.x, c.y);
    };

    public static Function<ArrayList<Point>, ArrayList<Coord>> funcPointsToCords = ps ->{
        ArrayList<Coord> cs = new ArrayList<>();
        if(null == ps || ps.size() == 0)
            return cs;
        List<Coord>  cl = ps.stream().map(funcPointToCord).collect(Collectors.toList());
        cs.addAll(cl);
        return cs;
    };

    public static Function<LinkedList<Coordinate>, LinkedList<Coord>> funcCoordinatesToCoords = cods ->{
        LinkedList<Coord> cs = new LinkedList<>();
        if(null == cods || cods.size() == 0)
            return cs;
        List<Coord>  cl = cods.stream().map(funcCoordinateToCoord).collect(Collectors.toList());
        cs.addAll(cl);
        return cs;
    };


    /**
     *
     * @param poly the polygon geometry inside which the random point is to be found
     * @return
     */
    public static Coord randomPointInPolygon(Polygon poly)
    {
        try{
            Envelope env = poly.getEnvelopeInternal();
            double max = env.getMaxX()-env.getMinX();
            if((env.getMaxY()- env.getMinY()) > max)
                max = env.getMaxY()- env.getMinY();

            max *=2;

            //get the centre of the polygon
            Point centre = poly.getInteriorPoint();
            //chooses a random direction and create a line that intersects the polygon boundary
            double angle = rng.nextDouble() * 360;
            double x1 = centre.getX() + (max * Math.cos(Math.toRadians(angle)));
            double y1 = centre.getY() + (max * Math.sin(Math.toRadians(angle)));

            Coordinate[] longLine = new Coordinate[2];
            longLine[0] = new Coordinate(centre.getX(), centre.getY());
            longLine[1] = new Coordinate(x1, y1);
            //find the intersection with the polygon
            LineString ls = GEOMETRY_FACTORY.createLineString(longLine);

            Geometry boundary = poly.getBoundary();
            Geometry intersection = boundary.intersection(ls);
            Coordinate intersestCord = intersection.getCoordinate();
            double dist = DistanceOp.distance(centre, GEOMETRY_FACTORY.createPoint(intersestCord));

            //a random fraction of the length to the edge
            dist *= rng.nextDouble();

            double x = centre.getX() + (dist * Math.cos(Math.toRadians(angle)));
            double y = centre.getY() + (dist * Math.sin(Math.toRadians(angle)));

            return new Coord(x,y);
        }catch (Exception ex)
        {
            cmrra.data.index.utility.Log.error("randomPointInPolygon() "+ cmrra.data.index.utility.Log.getExceptionString(ex));
            throw ex;
        }
    }


    /**
     *
     * @param poly the polygon geometry inside which the random point is to be found
     * @return
     */
    public static Coord modifiedRandomDirection(Polygon poly, Coord curr)
    {
        try{
            Envelope env = poly.getEnvelopeInternal();
            double max = env.getMaxX()-env.getMinX();
            if((env.getMaxY()- env.getMinY()) > max)
                max = env.getMaxY()- env.getMinY();

            max *=2;

            //get the centre of the polygon
            //Point centre = poly.getInteriorPoint();
            Point centre = funcCordToPoint.apply(curr);
            //chooses a random direction and create a line that intersects the polygon boundary
            double angle = rng.nextDouble() * 360;
            double x1 = centre.getX() + (max * Math.cos(Math.toRadians(angle)));
            double y1 = centre.getY() + (max * Math.sin(Math.toRadians(angle)));

            Coordinate[] longLine = new Coordinate[2];
            longLine[0] = new Coordinate(centre.getX(), centre.getY());
            longLine[1] = new Coordinate(x1, y1);
            //find the intersection with the polygon
            LineString ls = GEOMETRY_FACTORY.createLineString(longLine);

            Geometry boundary = poly.getBoundary();
            Geometry intersection = boundary.intersection(ls);
            Coordinate intersestCord = intersection.getCoordinate();
            double dist = DistanceOp.distance(centre, GEOMETRY_FACTORY.createPoint(intersestCord));

            //a random fraction of the length to the edge
            dist *= rng.nextDouble();

            double x = centre.getX() + (dist * Math.cos(Math.toRadians(angle)));
            double y = centre.getY() + (dist * Math.sin(Math.toRadians(angle)));

            return new Coord(x,y);
        }catch (Exception ex)
        {
            cmrra.data.index.utility.Log.error("randomPointInPolygon() "+ cmrra.data.index.utility.Log.getExceptionString(ex));
            throw ex;
        }
    }

    public static boolean isPointInPolygon(Polygon poly, Point p)
    {
        return poly.contains(p);
    }

    public static Coord getClosestEdge(Geometry geom, Coord p)
    {
        Point pdash = funcCordToPoint.apply(p);
        Coordinate[] cd = DistanceOp.nearestPoints(geom, pdash);
        return funcCoordinateToCoord.apply(cd[0]);
    }

    public static Coord getOffShootPoint(Geometry geom1, Geometry geom2)
    {
        Coordinate[] cd = DistanceOp.nearestPoints(geom1, geom2);
        return funcCoordinateToCoord.apply(cd[0]);
    }

    /**
     *
     * @param geom the geometry to calculate distance from
     * @param p
     * @return
     */
    public static double getDistance(Geometry geom, Point p)
    {
        return geom.distance(p);
    }

    /**
     *Returns a sweeping path in a geometry.
     * Sweeping direction is the smallest side of the envelope
     * Movement direction is the longest side of the envelope
     * @param geom
     * @param sweepDistance
     * @return
     * TODO Change logic to accomodate linear features
     */
    public static LinkedList<Coordinate> getSweepingWaypoint(Geometry geom, double sweepDistance)
    {
        Envelope enc = geom.getEnvelopeInternal();

        Function<Double, LineString> nextLineCreator;
        double maxEdge;
        double minEdge;

        if((enc.getMaxY() - enc.getMinY()) > (enc.getMaxX() - enc.getMinX()))
        {
            maxEdge = enc.getMaxY();
            minEdge = enc.getMinY();
            nextLineCreator = c ->{
                Coordinate[] coordinates = new Coordinate[2];
                coordinates[0] = new Coordinate(enc.getMinX(), c);
                coordinates[1] = new Coordinate(enc.getMaxX(), c);
                return GEOMETRY_FACTORY.createLineString(coordinates);
            };
        }
        else
        {
            maxEdge = enc.getMaxX();
            minEdge = enc.getMinX();
            nextLineCreator = c ->{
                Coordinate[] coordinates = new Coordinate[2];
                coordinates[0] = new Coordinate(c, enc.getMinY());
                coordinates[1] = new Coordinate(c, enc.getMaxY());
                return GEOMETRY_FACTORY.createLineString(coordinates);
            };
        }
        int dir = 1;
        LinkedList<Coordinate> waypoints= new LinkedList<>();
        do{
            LineString line = nextLineCreator.apply(maxEdge);
            maxEdge -= sweepDistance;
            Geometry intersec = geom.intersection(line);
            Coordinate[] list = intersec.getCoordinates();

            //if narrower than sweeping width then choose the middle point.
            //for example in a pipeline.
            //though if the pipeline is at an angle, it might not take the middle point
             if(intersec.getLength() < sweepDistance*2)
             {
                 double xMid = (list[0].x+list[list.length -1].x)/2;
                 double yMid = (list[0].y+list[list.length -1].y)/2;
                 waypoints.add(new Coordinate(xMid, yMid));
                 continue;
             }

            if(dir ==1)
            {
                dir=-1;
                for(Coordinate c :list)
                {
                    waypoints.add(c);
                }
            }
            else
            {
                dir = 1;
                ArrayUtils.reverse(list);
                for(Coordinate c :list)
                {
                    waypoints.add(c);
                }
            }

        }while(maxEdge >= minEdge);

        return waypoints;
        //return funcCoordinatesToCoords.apply(waypoints);
    }

    /**
     * if x is one side of the square of the covered by a sensor at any time,
     * s the speed of how fast the centre of the square moves,
     * and t the time for which the square moves. We are trying to find the t
     * Total Area A = x((t*s)+x)
     * t*s + x = A/x
     * t = A/(x*s) - x/s
     * @param geom the geometry that need to be covered
     * @param speed the speed at which it will be cov. speed is on m/sec
     * @param sensorRange the furthest distance from the sensor that is recorded
     *                    if the sensor is a camera, then this value should be half the length of the side of the quadrilateral it coveres
     *                    if the sensor is a network comm module then the value should be the range if the module
     * @return the avg time that is needed to cover most of the region
     */
    public static double getCoverageTime(Geometry geom, double speed, double sensorRange)
    {
        try{
            LinkedList<Coordinate> waypoints = getSweepingWaypoint(geom, sensorRange*2);
            Coordinate[] ar = new Coordinate[waypoints.size()];
            ar = waypoints.toArray(ar);
            double covTime;
            if(ar.length < 2)
                return 300;

            LineString path = GEOMETRY_FACTORY.createLineString(ar);
            double pathlen = path.getLength();

            if(pathlen == 0)
            {
                covTime = 300; //wait 50 min at a point
            }
            else
            {
                covTime = (pathlen/speed) * 2; //for redundancy
            }

            return covTime;
        }catch (Exception ex)
        {
            cmrra.data.index.utility.Log.error("getCoverageTime() "+ cmrra.data.index.utility.Log.getExceptionString(ex));
            throw ex;
        }
    }

    public static LineString createLineString(List<Coord> pts)
    {
        if(pts.size() <2)
            return null;
        Coordinate[] ptsCrd = new Coordinate[pts.size()];
        List<Coordinate> lst = pts.stream().map(funcCoordToCoordinate).collect(Collectors.toList());
        ptsCrd = lst.toArray(ptsCrd);

        try{
            LineString line = GEOMETRY_FACTORY.createLineString(ptsCrd);
            return line;
        }catch (Exception ex)
        {
            cmrra.data.index.utility.Log.error("createLineString() "+ cmrra.data.index.utility.Log.getExceptionString(ex));
            throw ex;
        }
    }

    /**
     * Finds the common areas in the geometry
     * If the geometries are non overlapping then finds a middle ground between them
     * TODO: find better algorithm to find the common ground if no intersection is found
     * @param g1 first geometry. Expected Polygon, linestring, multilinestring etc
     * @param g2 second geometry. Expected Polygon, linestring, multilinestring etc
     * @return returns a geometry that is either common in both geometry or is a middle ground between them
     */
    public static Geometry intersectGeometry(Geometry g1, Geometry g2)
    {
        try{
            Geometry in;
            if(g1.intersects(g2))
            {
                in = g1.intersection(g2);
            }
            else{
                Coordinate[] cd = DistanceOp.nearestPoints(g1, g2);
                double x1 = cd[0].x;
                double x2 = cd[1].x;
                double y1 = cd[0].y;
                double y2 = cd[1].y;
                double dx = x1-x2;
                double dy = y1-y2;
                double dist = Math.sqrt(dx*dx + dy*dy);
                dx /= dist;
                dy /= dist;
                double rectLen = Math.sqrt(dx*dx + dy*dy) * 2;

                Coordinate[] rec = new Coordinate[5];
                rec[0] = new Coordinate(x1 + (rectLen/2)*dy,y1 - (rectLen/2)*dx);
                rec[1] = new Coordinate(x1 - (rectLen/2)*dy, y1 + (rectLen/2)*dx);
                rec[2] = new Coordinate(x2 - (rectLen/2)*dy, y2 + (rectLen/2)*dx);
                rec[3] = new Coordinate(x2 + (rectLen/2)*dy,y2 - (rectLen/2)*dx);
                rec[4] = new Coordinate(x1 + (rectLen/2)*dy,y1 - (rectLen/2)*dx);
                in = GEOMETRY_FACTORY.createPolygon(rec);
            }

            //if the intersection is not a polygon or a polygon or zero area then craete a better one
            if(in instanceof LineString || (in instanceof Polygon && in.getArea() == 0))
            {
                BufferOp buffOp = new BufferOp(in);
                in = (Polygon)buffOp.getResultGeometry(10); //10m buffer
            }
            else if(in instanceof Point)
            {
                Coordinate[] bd = new Coordinate[5];
                bd[0] = new Coordinate(((Point) in).getX(), ((Point) in).getY());
                bd[1] = new Coordinate(((Point) in).getX()+10, ((Point) in).getY());
                bd[2] = new Coordinate(((Point) in).getX()+10, ((Point) in).getY()+10);
                bd[3] = new Coordinate(((Point) in).getX(), ((Point) in).getY()+10);
                bd[4] = new Coordinate(((Point) in).getX(), ((Point) in).getY());
                in = GEOMETRY_FACTORY.createPolygon(bd);
            }
            return in;
        }catch (Exception ex)
        {
            cmrra.data.index.utility.Log.error("intersectGeometry() "+ cmrra.data.index.utility.Log.getExceptionString(ex));
            throw ex;
        }
    }
}
