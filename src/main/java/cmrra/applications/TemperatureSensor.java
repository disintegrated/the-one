package cmrra.applications;

import cmrra.configuration.XMLConfigurationReaderWriter;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import cmrra.entity.data.TemperatureData;
import core.*;
import cmrra.data.index.factories.Geometryfactory;
import java.util.Random;
import java.util.UUID;

/**
 * Created by rabbiddog on 11.11.17.
 */
public class TemperatureSensor extends Application{

    public final int interval;
    private int _lastReading;
    private static Random rnd = new Random();

    public TemperatureSensor(Integer in)
    {
        this.interval = in;
        _lastReading = 0 ;
        appID = XMLConfigurationReaderWriter.Instance()
                .configuration.appConfigurations.stream()
                .filter(a -> a.appName.equals("TemperatureSensor")).findFirst().get().appID;
    }

    public TemperatureSensor(TemperatureSensor proto)
    {
        this.interval = proto.interval;
        _lastReading = 0 ;
        this.appID = proto.appID;
    }

    @Override
    public Message handle(Message msg, DTNHost host) {
        return null;
    }

    @Override
    public void update(DTNHost host)
    {
        try
        {
            int currTime = SimClock.getIntTime();
            if((currTime - _lastReading) > interval)
            {
                double reading = rnd.nextGaussian() * rnd.nextInt(50);
                String ID = UUID.randomUUID().toString();
                Coord loc = host.getLocation();
                Sp2DBiTemporalBoundary boundary =
                        Geometryfactory.createBoundary(loc.getX(), loc.getX(), loc.getY()
                                , loc.getY(), currTime, cmrra.data.index.entity.Constants.TRANSACTION_UC
                                , currTime, currTime );
                TemperatureData data = new TemperatureData(host.getName()+appID+ID, boundary, reading);
                host.store().add(data);
                _lastReading = currTime;
            }
        }catch (Exception ex)
        {
            cmrra.data.index.utility.Log.error(cmrra.data.index.utility.Log.getExceptionString(ex));
            ex.printStackTrace();
            throw new SimError(ex);
        }
    }

    @Override
    public Application replicate() {
        return new TemperatureSensor(this);
    }
}
