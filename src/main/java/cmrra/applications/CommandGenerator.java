package cmrra.applications;

import cmrra.configuration.XMLConfigurationReaderWriter;
import cmrra.data.index.factories.Geometryfactory;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import cmrra.entity.CachedMapFeatureStore;
import cmrra.entity.MapFeature;
import cmrra.entity.config.MapLayout;
import cmrra.entity.data.Command;
import cmrra.utility.geometry.GeometryUtil;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import core.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.text.RandomStringGenerator;

/**
 * Used by base stations to generate commands for nodes on the field
 */
public class CommandGenerator  extends Application {

    public final List<Polygon> regionsOfSurvey;
    private int _lastReading;
    public final int interval;
    private static Random rnd = new Random();
    private static RandomStringGenerator  rgn = new RandomStringGenerator.Builder()
            .withinRange('a', 'z').build();

    public CommandGenerator(Integer in)
    {
        interval = in;
        _lastReading = 0 ;
        List<MapLayout> ground = XMLConfigurationReaderWriter.Instance()
                .configuration.mapLayouts.stream().filter(m -> m.isRegion).collect(Collectors.toList());
        regionsOfSurvey = new ArrayList<>();
        for (MapLayout layout:ground)
        {
            Geometry feature = CachedMapFeatureStore.Instance().getFeature(layout.featureId).feature;
            if(feature instanceof MultiPolygon)
                throw new IllegalArgumentException("Multi-Polygol is not fully supported yet");
            if(feature instanceof Polygon)
                regionsOfSurvey.add((Polygon)feature);
        }
        appID = XMLConfigurationReaderWriter.Instance()
                .configuration.appConfigurations.stream()
                .filter(a -> a.appName.equals("CommandGenerator")).findFirst().get().appID;
    }

    public CommandGenerator(CommandGenerator cg)
    {
        regionsOfSurvey = cg.regionsOfSurvey;
        interval = cg.interval;
        _lastReading = 0 ;
        this.appID = cg.appID;
    }
    @Override
    public Message handle(Message msg, DTNHost host) {
        return null;
    }

    @Override
    public void update(DTNHost host) {
        try
        {
            int currTime = SimClock.getIntTime();
            if((currTime - _lastReading) > interval)
            {
                //for each sub region generate a command
                for (Polygon p:regionsOfSurvey)
                {
                    String ID = UUID.randomUUID().toString();
                    Coord loc = GeometryUtil.randomPointInPolygon(p);
                    Sp2DBiTemporalBoundary boundary =
                            Geometryfactory.createBoundary(loc.getX(), loc.getX(), loc.getY()
                                    , loc.getY(), currTime, cmrra.data.index.entity.Constants.TRANSACTION_UC
                                    , currTime, currTime +  108000); //valid for 30hr
                    String s = rgn.generate(100);
                    Command cmd = new Command(host.getName()+appID+ID, boundary,s);
                    host.store().add(cmd);
                }
                _lastReading = currTime;
            }
        }catch (Exception ex)
        {
            cmrra.data.index.utility.Log.error(cmrra.data.index.utility.Log.getExceptionString(ex));
            ex.printStackTrace();
            throw new SimError(ex);
        }
    }

    @Override
    public Application replicate() {
        return new CommandGenerator(this);
    }
}
