package cmrra.movement;

import cmrra.entity.HostTask;
import core.Coord;

/**
 * Created by rabbiddog on 06.08.17.
 */
public interface ITaskCapable {

    //when a new task is assigned or need to be started afresh
    boolean assignTask(HostTask task);
    //current task did not complete but the CMRRA is switching to another task and states of this task need not be saved
    boolean abortTask();
    //purge saved state of all previously saved or running task
    boolean abortAllTasks();
    //CMRRA switched to a different task with intention to resume current task.
    boolean pauseTask();
    //resume a previously paused task. Need specific ID as multiple tasks might be paused
    boolean resumeTask(String taskId,Coord lastPosition);
    //stop the running threads. Called when closing application
    void stopServicing();
}
