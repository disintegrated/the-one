package cmrra.movement;

import cmrra.configuration.XMLConfigurationReaderWriter;
import cmrra.data.index.utility.Log;
import cmrra.entity.*;
import cmrra.entity.config.*;
import cmrra.utility.geometry.GeometryUtil;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.math.Vector2D;
import core.*;
import movement.MovementModel;
import movement.Path;
import movement.map.SimMap;
import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.*;
import java.util.Enumeration;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * Created by rabbiddog on 18.07.17.
 */
public class CMRRAMovement extends MovementModel {

    public static final String MODIFIED_RDM = "MRDMM";
    public static final String PROBABILISTIC = "PBMM";
    public static final String SCHEDULED = "SCHMM";
    public static final String SPATIALLY_CONSTRAINST = "SPCOMM";
    public static final String SURVEY = "SRVMM";
    public static final String LINEAR = "LNRMM";

    public String TAG = "Class: CMRRAMovement.";

    protected Group groupSetting;
    //stores the featureIds of the known stations in the surveyed region for quick retrieval
    protected ArrayList<String> featureIdStations;
    protected HashMap<String, MapFeature> featureStations;

    /*Node contrainsts*/
    Battery battery;
    private String currentRestingStationId;
    protected HostOperationState hostOperationState;
    protected HostTask currentRunningTask;
    protected HashSet<HostTask> queuedTasks;
    protected HashMap<String, NodeContactSchedule> contactNodeSchedule;
    protected HashMap<String, HopContactSchedule> contactHopSchedule;
    protected FeatureSurveySchedule surveyFeatureSchedule;
    protected WanderRegionSchedule wanderRegionSchedule;
    protected static HashMap<String, DTNHost> neighborHosts;

    public ConcurrentHashMap<String, MovementModel> movementModels;
    protected double maximumTransmitRange = 0.0;
    private boolean hasBootstrapped;
    private static  Random rng = new Random();
    private ContactNodeTask remoteOverdueInitiated = null;

    public CMRRAMovement()
    {
        super();
        movementModels = new ConcurrentHashMap<>();
        neighborHosts = new HashMap<>();
        //create the child movement models. Right now all. Later read from configuration
        movementModels.put(MODIFIED_RDM, new ModifiedRandomDirectionMovement());
        movementModels.put(SCHEDULED, new ScheduledMapMovement());
        movementModels.put(SPATIALLY_CONSTRAINST, new SpatiallyConstrainedMovement());
        movementModels.put(PROBABILISTIC, new ProbabilisticMovement());
        movementModels.put(SURVEY, new SurveyMovement());
        movementModels.put(LINEAR, new StraightToWaypointMovement());
    }

    public CMRRAMovement(CMRRAMovement mm)
    {
        super(mm);
        movementModels = new ConcurrentHashMap<>();
        Enumeration<String> e = mm.movementModels.keys();
        while (e.hasMoreElements())
        {
            String key = e.nextElement();
            movementModels.put(key, mm.movementModels.get(key).replicate());
        }

       featureIdStations = new ArrayList<>();
       featureStations = new HashMap<>();
       queuedTasks = new HashSet<>();
       hasBootstrapped = false;
    }

    //called for each new movement model object created for a DTNHost
    public void lateInit(String groupId)
    {
        super.lateInit(groupId);
        groupSetting = XMLConfigurationReaderWriter.Instance().configuration.hostGroups
                .stream().filter(s -> s.groupId == groupId).findFirst().get();

        TAG =TAG+"host: "+host.getName()+" ";
        contactNodeSchedule = new HashMap<>();
        contactHopSchedule = new HashMap<>();
        for (ComInterface cI:XMLConfigurationReaderWriter.Instance().configuration.comInterfaces)
        {
            if(maximumTransmitRange==0.0)
                maximumTransmitRange = cI.transmitRange;
            else if(maximumTransmitRange< cI.transmitRange)
            {
                maximumTransmitRange =cI.transmitRange;
            }
        }

        //init child movement models
        //ugly method
        SurveyMovement SRVMM = (SurveyMovement)movementModels.get(SURVEY);
        SRVMM.setHost(host);
        SRVMM.lateInit(groupId);

        StraightToWaypointMovement STRMM = (StraightToWaypointMovement)movementModels.get(LINEAR) ;
        STRMM.setHost(host);
        STRMM.lateInit(groupId);

        ModifiedRandomDirectionMovement MRDMM = (ModifiedRandomDirectionMovement)movementModels.get(MODIFIED_RDM);
        MRDMM.setHost(host);
        MRDMM.lateInit(groupId);


        ScheduledMapMovement SCHMM = (ScheduledMapMovement)movementModels.get(SCHEDULED);
        SCHMM.setHost(host);
        SCHMM.lateInit(groupId); //this will schedule the first survey task already


        SpatiallyConstrainedMovement SPCOMM = (SpatiallyConstrainedMovement)movementModels.get(SPATIALLY_CONSTRAINST);
        SPCOMM.setHost(host);
        SPCOMM.lateInit(groupId);


        ProbabilisticMovement PBMM = (ProbabilisticMovement)movementModels.get(PROBABILISTIC);
        PBMM.setHost(host);
        PBMM.lateInit(groupId);


        MapLayout layout = XMLConfigurationReaderWriter.Instance().configuration.mapLayouts.stream()
                .filter(s -> s.featureId.compareTo(groupSetting.surveyedRegion) == 0).findFirst().get();

        if(null!= layout && null != layout.stationIds && layout.stationIds.size() >0)
        {
            featureIdStations.addAll(layout.stationIds);
        }

        for (String stId: featureIdStations)
        {
            MapFeature st = CachedMapFeatureStore.Instance().getFeature(stId);
            if(null != st)
            {
                featureStations.put(stId, st);
            }
            else
            {
                _log.error(TAG+"Could not find Mapfeature in CachedMapFeatureStore for featureId "+stId);
            }
        }

        battery = new Battery(groupSetting.nodeFeatureConstraints.batteryLife*60,groupSetting.nodeFeatureConstraints.rechargeTime*60, this.host.getName()); //convert time into seconds
        //add the known tasks to the list
        //Add the FeatureSurveySchedule for the region
        //this has a predefined path which does not change
        if(groupSetting.nodeFeatureConstraints.isDrone)
        {
            surveyFeatureSchedule =new FeatureSurveySchedule(groupSetting.surveyedRegion, CachedMapFeatureStore.Instance().getFeature(groupSetting.surveyedRegion), MovementPattern.fromInteger(groupSetting.surveyPattern));
            surveyFeatureSchedule.interval = groupSetting.surveyInterval;
            surveyFeatureSchedule.lastTaskCompletionAt = -groupSetting.surveyInterval;
            if(surveyFeatureSchedule.movementPattern == MovementPattern.SWEEP)
            {
                LinkedList<Coordinate> waypoints = GeometryUtil.getSweepingWaypoint(surveyFeatureSchedule.feature.feature, groupSetting.surveyPathDensity);
                surveyFeatureSchedule.setPath(GeometryUtil.funcCoordinatesToCoords.apply(waypoints));
            }
            else {
                surveyFeatureSchedule.taskDuration = GeometryUtil.getCoverageTime(surveyFeatureSchedule.feature.feature, (groupSetting.speed.pos2 + groupSetting.speed.pos1) /2, maximumTransmitRange);
            }

            hostOperationState = HostOperationState.WAIT_ASSIGNMENT;
        }
        else{
            wanderRegionSchedule = new WanderRegionSchedule(CachedMapFeatureStore.Instance().getFeature(groupSetting.surveyedRegion), groupSetting.surveyInterval, groupSetting.groundNodeActivityDuration);
            wanderRegionSchedule.lastTaskCompletionAt = -wanderRegionSchedule.restDuration;
            hostOperationState = HostOperationState.WAIT_ASSIGNMENT;
        }
    }

    @Override
    public Path getPath()
    {
        return null;
    }

    public DoubleTuple takeNextStep(double timeIncrement)
    {
        try
        {
            if(groupSetting.nodeFeatureConstraints.isDrone)
            {
                if(!hasBootstrapped)
                {
                    bootstrapNeighbors();
                }
                if(hostOperationState == HostOperationState.RECHARGE_BATTERY)
                {
                    if(battery.isBatteryFull())
                    {
                        hostOperationState = HostOperationState.WAIT_ASSIGNMENT;
                    }
                    else
                    {
                        battery.rechargeFor(timeIncrement);
                    }
                }
                else if(hostOperationState == HostOperationState.GO_HOME)
                {

                }
                if(hostOperationState == HostOperationState.WAIT_ASSIGNMENT) {
                    if (batteryNeedsRecharge(timeIncrement, groupSetting.speed.pos2))
                    {
                        hostOperationState = HostOperationState.GO_HOME;
                        currentRunningTask = new GoHomeTask(featureStations.get(currentRestingStationId).coords().get(0), HostTask.getHashId());
                    }
                    else
                    {
                        HostTask surveyTask = null;
                        Optional<HostTask> optional = queuedTasks.stream().filter(s -> s.taskType == TaskType.SURVEY).findAny();
                        if (optional.isPresent())
                            surveyTask = optional.get();
                        if (null != surveyTask)
                        {
                            currentRunningTask = surveyTask;
                            queuedTasks.remove(surveyTask);
                            hostOperationState = HostOperationState.SURVEY_REGION;
                        }//check if time for starting next survey
                        else if (null == surveyTask && (SimClock.getTime() - surveyFeatureSchedule.lastTaskCompletionAt) > surveyFeatureSchedule.interval)
                        {
                            if(surveyFeatureSchedule.movementPattern == MovementPattern.SWEEP)
                            {
                                LinkedList<Coord> path = surveyFeatureSchedule.getPath();
                                Coord startLoc = this.host.getLocation();
                                Coord fst = path.getFirst();
                                Coord lst = path.getLast();

                                double distToFst = fst.distance(startLoc);
                                double distToLst = lst.distance(startLoc);
                                if(distToFst > distToLst)
                                {
                                    Coord[] temp = new Coord[path.size()];
                                    temp = path.toArray(temp);
                                    ArrayUtils.reverse(temp);
                                    path = new LinkedList<Coord>(Arrays.asList(temp));
                                }
                                surveyTask = new SweepTask(TaskType.SURVEY, HostTask.getHashId(), surveyFeatureSchedule.featureId, path);
                            }
                            else if(surveyFeatureSchedule.movementPattern == MovementPattern.RANDOM)
                            {
                                surveyTask = new WanderTask(TaskType.SURVEY, HostTask.getHashId(),  (Polygon) surveyFeatureSchedule.feature.feature, surveyFeatureSchedule.taskDuration);
                            }

                            currentRunningTask = surveyTask;
                            hostOperationState = HostOperationState.SURVEY_REGION;
                        } else {
                            HashSet<String> allNodesScheduledForContact = new HashSet<>();
                            queuedTasks.stream().forEach(s -> {
                                if (s instanceof ContactNodeTask) {
                                    ContactNodeTask ct = (ContactNodeTask) s;
                                    allNodesScheduledForContact.add(ct.target);
                                }
                            });
                            scanForOtherTasks(allNodesScheduledForContact);

                            if (queuedTasks.size() > 0) {
                                HostTask p = getPrioritytask();
                                if (null != p) {
                                    hostOperationState = HostOperationState.CONTACT_NODE;
                                    currentRunningTask = p;
                                    queuedTasks.remove(p);
                                }
                            }
                        }
                    }
                }
                if(hostOperationState == HostOperationState.SURVEY_REGION)
                {
                    if(batteryNeedsRecharge(timeIncrement, groupSetting.speed.pos2))
                    {
                        if(surveyFeatureSchedule.movementPattern == MovementPattern.SWEEP)
                        {
                            hostOperationState = HostOperationState.GO_HOME;
                            SweepTask srvtk = (SweepTask)currentRunningTask;
                            srvtk.pause();
                            srvtk.path.add(this.host.getLocation());//save current location for resuming
                        }
                        else if(surveyFeatureSchedule.movementPattern == MovementPattern.RANDOM)
                        {
                            hostOperationState = HostOperationState.GO_HOME;
                            WanderTask wndrtk = (WanderTask) currentRunningTask;
                            wndrtk.pause();
                        }
                        queuedTasks.add(currentRunningTask);
                        currentRunningTask = new GoHomeTask(featureStations.get(currentRestingStationId).coords().get(0), HostTask.getHashId());
                    }
                    else
                    {
                        HashSet<String> allNodesScheduledForContact = new HashSet<>();
                        queuedTasks.stream().forEach(s -> {
                            if(s instanceof ContactNodeTask)
                            {
                                ContactNodeTask ct =(ContactNodeTask)s;
                                allNodesScheduledForContact.add(ct.target);
                            }
                        });
                        scanForOtherTasks(allNodesScheduledForContact);

                        if(surveyFeatureSchedule.movementPattern == MovementPattern.SWEEP)
                        {
                            for (HostTask h:queuedTasks)
                            {
                                if(h instanceof ContactNodeTask)
                                    snapTaskOnSurveyPath((SweepTask) currentRunningTask, (ContactNodeTask) h);
                            }
                        }
                    }
                }
                if(hostOperationState == HostOperationState.CONTACT_NODE)
                {
                    if(batteryNeedsRecharge(timeIncrement, groupSetting.speed.pos2))
                    {
                        hostOperationState = HostOperationState.GO_HOME;
                        currentRunningTask.pause();
                        queuedTasks.add(currentRunningTask);
                        currentRunningTask = new GoHomeTask(featureStations.get(currentRestingStationId).coords().get(0), HostTask.getHashId());
                    }
                }

                DoubleTuple step = new DoubleTuple(0,0);
                switch (hostOperationState)
                {
                    case GO_HOME:
                        battery.runFor(timeIncrement);
                        /*
                        if(this.host.getGroup() == "g7" || this.host.getGroup() == "g6")
                        {
                            _log.debug("pause");
                        }
                        */
                        if(!(currentRunningTask instanceof GoHomeTask))
                        {
                            _log.error(TAG + "Expecting task of type GoHomeTask but instead encountered "+currentRunningTask.getClass());
                            throw new SimError(TAG + "Expecting task of type GoHomeTask but instead encountered "+currentRunningTask.getClass());
                        }

                        StraightToWaypointMovement STRMM = (StraightToWaypointMovement)movementModels.get(LINEAR) ;
                        step = STRMM.takeNextStep(timeIncrement, (GoHomeTask) currentRunningTask);

                        if(currentRunningTask.taskState == HostTaskState.COMPLETED)
                        {
                            currentRunningTask = null;
                            hostOperationState = HostOperationState.RECHARGE_BATTERY;
                        }
                        break;
                    case CONTACT_NODE:
                        battery.runFor(timeIncrement);

                        ContactNodeTask tk = (ContactNodeTask)currentRunningTask;
                        //if task just started then readjust the duration to include travel time
                        //for polygon, the movement model already takes care of travel time
                        if(tk.taskState != HostTaskState.RUNNING && tk.expectedLoc instanceof Point)
                            readjustDuration(tk);
                        if(tk.expectedLoc instanceof Point){
                            STRMM = (StraightToWaypointMovement)movementModels.get(LINEAR) ;
                            step = STRMM.takeNextStep(timeIncrement, tk);
                        }
                        else {
                            ModifiedRandomDirectionMovement MRDMM = (ModifiedRandomDirectionMovement)movementModels.get(MODIFIED_RDM);
                            step = MRDMM.takeNextStep(timeIncrement, (ContactNodeTask)currentRunningTask);
                        }
                        HashMap<DTNHost, Coord>  contactedHost = nodesInContact(this.host.getLocation(), step);

                        //run throught the list of hosts contacted and update their contact information
                        for (Map.Entry<DTNHost, Coord> ht: contactedHost.entrySet())
                        {
                            DTNHost theHost = ht.getKey();
                            if(theHost.getName().equals(tk.target))
                            {
                                tk.taskState = HostTaskState.COMPLETED;
                                break;
                            }
                        }

                        if(currentRunningTask.taskState == HostTaskState.COMPLETED)
                        {
                            //update attempted time for hop
                            if(contactHopSchedule.containsKey(tk.target))
                            {
                                HopContactSchedule hsc = contactHopSchedule.get(tk.target);
                                hsc.lastContactAttemptAt = SimClock.getTime();
                            }
                            else if(contactNodeSchedule.containsKey(tk.target))
                            {
                                NodeContactSchedule nsc = contactNodeSchedule.get(tk.target);
                                nsc.lastTaskCompletionAt = SimClock.getTime();
                            }
                            currentRunningTask = null;
                            hostOperationState = HostOperationState.WAIT_ASSIGNMENT;
                        }
                        setHostAsContacted(contactedHost);
                        break;
                    case WAIT_ASSIGNMENT:
                        //do we send the host to home or wait at location??
                        //efficient solution will depend on the next task but is it possible to know that in advance?
                        battery.runFor(timeIncrement);
                        break;
                    case SURVEY_REGION:
                        battery.runFor(timeIncrement);

                        if(surveyFeatureSchedule.movementPattern == MovementPattern.SWEEP)
                        {
                            SweepTask srvTk = (SweepTask) currentRunningTask;
                            SurveyMovement SRVMM = (SurveyMovement) movementModels.get(SURVEY);
                            step = SRVMM.takeNextStep(timeIncrement, srvTk);
                        }
                        else if(surveyFeatureSchedule.movementPattern == MovementPattern.RANDOM)
                        {
                            WanderTask srvTk = (WanderTask)currentRunningTask;
                            ModifiedRandomDirectionMovement MRDMM = (ModifiedRandomDirectionMovement)movementModels.get(MODIFIED_RDM);
                            step = MRDMM.takeNextStep(timeIncrement,  srvTk);
                        }
                        contactedHost = nodesInContact(this.host.getLocation(), step);
                        setHostAsContacted(contactedHost);
                        if(currentRunningTask.taskState == HostTaskState.COMPLETED)
                        {
                            surveyFeatureSchedule.lastTaskCompletionAt = SimClock.getTime();
                            currentRunningTask = null;
                            hostOperationState = HostOperationState.WAIT_ASSIGNMENT;
                        }
                        else //check for sub tasks
                        {
                            Coord currLoc = this.host.getLocation();
                            Coord finalPos = new Coord(currLoc.getX()+step.pos1, currLoc.getY()+step.pos2);

                            ContactNodeTask overdue = null;
                            //check if another node started a contact procedure
                            if(remoteOverdueInitiated != null)
                            {
                                overdue = remoteOverdueInitiated;
                                remoteOverdueInitiated = null;
                                //update schedule for attempt made
                            }

                            //if a hop needs to be contacted give that priority
                            if(null == overdue)
                            {
                                overdue = getOverdueHopContact();
                                if(!informRemoteOfOverdueContact(overdue))
                                    overdue = null;
                            }

                            if(null != overdue)
                            {
                                if(surveyFeatureSchedule.movementPattern == MovementPattern.SWEEP)
                                {
                                    ((SweepTask) currentRunningTask).path.add(0, finalPos);//save location in path for resuming
                                }

                                queuedTasks.add(currentRunningTask);
                                currentRunningTask = overdue;
                                queuedTasks.remove(overdue);
                                hostOperationState = HostOperationState.CONTACT_NODE;
                            }
                            else
                            {
                                double possibleMovement = currLoc.distance(finalPos);
                                LinkedList<Coord> endPts = new LinkedList<>();
                                endPts.add(currLoc);
                                endPts.add(finalPos);
                                //else check what task is best suited to be done from this location
                                //treat hops and ground nodes with same priority
                                if(endPts.size() != 2)
                                {
                                    Log.error("not enough");
                                }
                                LineString flight = GeometryUtil.createLineString(endPts);
                                if(surveyFeatureSchedule.movementPattern == MovementPattern.SWEEP)
                                {
                                    SweepTask srvTk = (SweepTask) currentRunningTask;
                                    //if the movement moved close to coord from where another sub task can be started
                                    // then after this step, pause task and switch to sub task
                                    Coord nextTkCoordKey = null;
                                    for (Map.Entry<String, Coord> et:srvTk.subTasks.entrySet())
                                    {
                                        Coord subTkLoc = et.getValue();
                                        String subTkId = et.getKey();
                                        double d = GeometryUtil.getDistance(flight, GeometryUtil.funcCordToPoint.apply(subTkLoc));
                                        //a step might make the drone cross over the sub task start point
                                        if(d <= possibleMovement )
                                        {
                                            Optional<HostTask> optional = queuedTasks.stream().filter(t -> t.unqId == subTkId).findFirst();
                                            HostTask ht = null;
                                            if(optional.isPresent())
                                                ht = optional.get();

                                            //if tasks were removed because the nodes where contacted on the way
                                            if(null == ht || !(ht instanceof ContactNodeTask))
                                            {
                                                continue;
                                            }
                                            //to avoid the drone going back to a point in the opposite direction
                                            else if(!isInGeneralDirectionOfMovement(step, (ContactNodeTask) ht))
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                srvTk.path.add(0, finalPos); //save location in path for resuming
                                                queuedTasks.add(currentRunningTask);
                                                currentRunningTask = ht;
                                                queuedTasks.remove(ht);
                                                hostOperationState = HostOperationState.CONTACT_NODE;
                                                nextTkCoordKey = subTkLoc;
                                            }
                                        }
                                        break;
                                    }
                                    if(null != nextTkCoordKey)
                                        srvTk.subTasks.remove(nextTkCoordKey);
                                }
                                //optimize random movement and coverage when multiple drones deployed in same region
                                //hop contacts are still maintained by the HopContact Schedule
                                /*else if(surveyFeatureSchedule.movementPattern == MovementPattern.RANDOM)
                                {
                                    List<ContactNodeTask> subTasks = queuedTasks.stream().filter(s -> s instanceof ContactNodeTask).map(s -> (ContactNodeTask)s).collect(Collectors.toList());
                                    for (ContactNodeTask ctntk:subTasks)
                                    {

                                    }
                                }*/
                            }
                        }

                        break;
                    default:
                        break;
                }
                return step;
            }
            else
            {
                //battery is assumed infinite or the movement model does notcare about battery life
                DoubleTuple step = new DoubleTuple(0.0, 0.0);
                if(hostOperationState == HostOperationState.WAIT_ASSIGNMENT)
                {
                    if(SimClock.getTime()- wanderRegionSchedule.lastTaskCompletionAt >wanderRegionSchedule.restDuration)
                    {
                        currentRunningTask = new WanderTask(TaskType.WANDER, HostTask.getHashId(), (Polygon)wanderRegionSchedule.wanderFeature.feature, wanderRegionSchedule.wanderDuration);
                        hostOperationState = HostOperationState.WANDER;
                    }
                }
                if(hostOperationState == HostOperationState.WANDER)
                {
                    WanderTask wanderTask = (WanderTask)currentRunningTask;
                    ModifiedRandomDirectionMovement MRDMM = (ModifiedRandomDirectionMovement)movementModels.get(MODIFIED_RDM);
                    step = MRDMM.takeNextStep(timeIncrement,  wanderTask);
                    if(wanderTask.taskState ==  HostTaskState.COMPLETED)
                    {
                        wanderRegionSchedule.lastTaskCompletionAt = SimClock.getTime();
                        currentRunningTask = null;
                        hostOperationState = HostOperationState.WAIT_ASSIGNMENT;
                    }
                }
                return step;
            }
        }catch (Exception ex)
        {
            _log.error(TAG +"Error while setting next location. Message: "+ex.getMessage()+". Stack: "+ex.getStackTrace());
            throw new SimError(TAG +"Error while setting next location.", ex);
        }

    }

    @Override
    public Coord getInitialLocation()
    {
        if(groupSetting.nodeFeatureConstraints.isDrone)
        {
            String regionId = groupSetting.surveyedRegion;
            MapLayout layout = XMLConfigurationReaderWriter.Instance().configuration.mapLayouts
                    .stream().filter(s -> s.featureId.equals(regionId)).findFirst().get();
            MapFeature station;
            if(null != layout.baseStationId && !layout.baseStationId.isEmpty())
            {
                station = CachedMapFeatureStore.Instance().getFeature(layout.baseStationId);
            }
            else
            {
                station = CachedMapFeatureStore.Instance().getFeature(layout.stationIds.get(0));
            }
            return station.coords().get(0);
        }
        else
        {
           //place randomly around the region. Points might be outside the tight polygon
            String regionId = groupSetting.surveyedRegion;
            MapFeature regionMapFeature =CachedMapFeatureStore.Instance().getFeature(regionId);
            if(!(regionMapFeature.feature instanceof Polygon))
                throw  new SimError("The map feature with id:"+regionId+" should be a polygon");
            return GeometryUtil.randomPointInPolygon((Polygon) regionMapFeature.feature);
        }
    }

    @Override
    public MovementModel replicate() {
        return new CMRRAMovement(this);
    }

    public SimMap getMap()
    {
       return CachedMapFeatureStore.Instance().getMap();
    }

    protected boolean batteryNeedsRecharge(double timeIncrement, double speed)
    {
        try
        {
            if(battery.batteryPercentage() >= 50.0)
                return false;
            Coord closestStation = null;
            double closestDistance = 0.0;
            Coord hostLoc = host.getLocation();
            if(featureIdStations.size() == 0)
            {
                throw new Exception("Region has not station defined for battery recgarge");
            }
            else if(featureIdStations.size() == 1)
            {
                currentRestingStationId = featureIdStations.get(0);
                closestStation = featureStations.get(featureIdStations.get(0)).coords().get(0);
                closestDistance = GeometryUtil.funcCordToPoint.apply(closestStation).distance(GeometryUtil.funcCordToPoint.apply(hostLoc));
            }
            else
            {
                for (String stId:featureIdStations)
                {
                    MapFeature f = featureStations.get(stId);
                    Coord loc = f.coords().get(0); //should have only one item
                    double d = loc.distance(hostLoc);

                    if(null == closestStation)
                    {
                        closestStation = loc;
                        closestDistance = d;
                        currentRestingStationId = stId;
                    }
                    else if(d<closestDistance)
                    {
                        closestStation = loc;
                        closestDistance = d;
                        currentRestingStationId = stId;
                    }
                }
            }

            if((battery.get_batteryLevel()-timeIncrement) > (closestDistance/speed))
                return false;
            else
            {
                return true;
            }

        }catch (Exception ex)
        {
            _log.error(TAG +"Error occured trying to figure if battery is sufficient. Message: "+ex.getMessage()+". Stack: "+ex.getStackTrace());
            throw new SimError(TAG + "Error occured trying to figure if battery is sufficient", ex);
        }
    }

    protected void snapTaskOnSurveyPath(SweepTask st, ContactNodeTask ht)
    {
        //if on last leg then only one coord is left on the path. Handle accordingly
        if(st.path.size() < 2)
        {
            return;
        }
        List<Coordinate> pathCord = st.path.stream().map(GeometryUtil.funcCoordToCoordinate).collect(Collectors.toList());
        Coordinate[] pathCordArr = new Coordinate[pathCord.size()];

        LineString path = GeometryUtil.GEOMETRY_FACTORY.createLineString(pathCord.toArray(pathCordArr));

        Coord offShootPointOnPath = GeometryUtil.getOffShootPoint(path, ht.expectedLoc);
        st.addSubTask(offShootPointOnPath, ht);
    }

    protected void scanForOtherTasks(HashSet<String> excludeHosts)
    {
        for (NodeContactSchedule nc:contactNodeSchedule.values())
        {
            if(excludeHosts.contains(nc.nodeId))
                continue;
            //skip if location not known yet
            //contact areas are decided after first contact is made and/or expected areas of contact are successfully established
            if(null == nc.contactArea)
                continue;
            if((SimClock.getTime() - nc.lastTaskCompletionAt) >= nc.interval)
            {
                double duration = 1;
                if(nc.contactArea instanceof Point)
                {
                    //TODO: decide how long to spend on a location
                    duration = 60.0;
                }
                else if(nc.contactArea instanceof Polygon)
                {
                    duration = GeometryUtil.getCoverageTime(nc.contactArea, groupSetting.speed.pos2, maximumTransmitRange);
                }
                ContactNodeTask vt = new ContactNodeTask(TaskType.CONTACT_HOST, HostTask.getHashId(), nc.nodeId, nc.contactArea, duration);
                queuedTasks.add(vt);
            }
        }

        //TODO:in future switch the single point for region of contact with a hop for multiple possible geometry

        for (HopContactSchedule hc:contactHopSchedule.values())
        {
            if(excludeHosts.contains(hc.nodeId))
                continue;
            //skip if location not known yet
            if(null == hc.contactArea)
                continue;
            if((SimClock.getTime() - hc.lastContactAttemptAt) >= hc.interval.pos1)
            {
                double duration = 1;
                if(hc.contactArea instanceof Point)
                {
                    //TODO: decide how long to spend on a location
                    duration = 300.0;
                }
                else if(hc.contactArea instanceof Polygon)
                {
                    duration = GeometryUtil.getCoverageTime(hc.contactArea, groupSetting.speed.pos2, maximumTransmitRange);
                }
                ContactNodeTask vt = new ContactNodeTask(TaskType.CONTACT_HOST, HostTask.getHashId(), hc.nodeId, hc.contactArea, duration);

                queuedTasks.add(vt);
            }
        }
    }


    /**
     * Goes through the queued tasks to find the task that needs to be completed first
     * If a hop was not contacted way past its max interval value then pick that.
     * Otherwise pick the closest task
     * @return the task to take up right now
     * can be completed in the given time
     */
    protected HostTask getPrioritytask()
    {
        Coord currLoc = this.host.getLocation();
        HostTask possibleTk = null;
        double minDist = 0.0;
        HashSet<HostTask> set = new HashSet<>(queuedTasks);
        for (HostTask ht: set)
        {
            if(ht instanceof ContactNodeTask)
            {
                ContactNodeTask vt = (ContactNodeTask)ht;

                //if any of the nodes were contacted in between then remove the task
                    if(contactNodeSchedule.containsKey(vt.target))
                    {
                        NodeContactSchedule nc = contactNodeSchedule.get(vt.target);
                        if(SimClock.getTime() - nc.lastTaskCompletionAt < nc.interval)
                        {
                            queuedTasks.remove(ht);
                            continue;
                        }

                    }
                    else if (contactHopSchedule.containsKey(vt.target))
                    {
                        HopContactSchedule hc = contactHopSchedule.get(vt.target);
                        //if already made an unsuccessful attempt then try later
                        if(SimClock.getTime() - hc.lastContactAttemptAt < hc.interval.pos1)
                        {
                            queuedTasks.remove(ht);
                            continue;
                        }
                        //if way past the max value then select this task
                        else if(SimClock.getTime() - hc.lastContactAt > hc.interval.pos2)
                        {
                            possibleTk = ht;
                            break;
                        }
                    }

                //if any of the targets can be reached
                double dist = GeometryUtil.getDistance(vt.expectedLoc, GeometryUtil.funcCordToPoint.apply(currLoc));
                if(null == possibleTk)
                {
                    possibleTk =ht;
                    minDist = dist;
                }
                else if(dist< minDist)
                {
                    possibleTk = ht;
                    minDist = dist;
                }
            }
        }
        return possibleTk;
    }

    public static HashMap<DTNHost, Coord> getAllHostLocations(List<String> except)
    {
        getAllHosts();
        HashMap<DTNHost, Coord> hostLocations = new HashMap<>();
        for (DTNHost hst:neighborHosts.values())
        {
            if(null != except && !except.contains(hst.getName()))
            {
                hostLocations.put(hst, hst.getLocation());
            }
        }

        return hostLocations;
    }
    
    public static HashMap<String, DTNHost> getAllHosts()
    {
        if(neighborHosts.size() == 0)
        {
            List<DTNHost> hosts =  SimScenario.getInstance().getHosts();
            for (DTNHost h:hosts)
            {
                neighborHosts.put(h.getName(), h);
            }
        }
        return neighborHosts;
    }
    protected void setHostAsContacted(HashMap<DTNHost, Coord> contactedHost)
    {
        if(contactedHost.size() == 0)
            return;
        //run through the list of hosts contacted and update their contact information
        for (Map.Entry<DTNHost, Coord> ht: contactedHost.entrySet())
        {
            DTNHost theHost = ht.getKey();
            if(groupSetting.groundNodeGroups != null && groupSetting.groundNodeGroups.size() > 0 && groupSetting.groundNodeGroups.contains(theHost.getGroup()))
            {
                if(contactNodeSchedule.containsKey(theHost.getName()))
                {
                    NodeContactSchedule nsc = contactNodeSchedule.get(theHost.getName());
                    nsc.contactArea = GeometryUtil.funcCordToPoint.apply( ht.getValue());
                    nsc.lastTaskCompletionAt = SimClock.getTime();
                }
                else
                {
                    NodeContactSchedule nsc = new NodeContactSchedule(theHost.getName(), groupSetting.groundNodeGroupsContactInterval, GeometryUtil.funcCordToPoint.apply( ht.getValue()));
                    nsc.lastTaskCompletionAt = SimClock.getTime();
                    contactNodeSchedule.put(theHost.getName(), nsc);
                }
            }
            else if(groupSetting.hopNodeGroups != null && groupSetting.hopNodeGroups.size() > 0 && groupSetting.hopNodeGroups.contains(theHost.getGroup()))
            {
                HopContactSchedule hsc;
                if(contactHopSchedule.containsKey(theHost.getName()))
                {
                    hsc = contactHopSchedule.get(theHost.getName());
                }
                else
                {
                    hsc = new HopContactSchedule(theHost.getName(), groupSetting.hopContactInterval, null);
                    contactHopSchedule.put(theHost.getName(), hsc);
                }
                hsc.lastContactAttemptAt = SimClock.getTime();
                hsc.lastContactAt = SimClock.getTime();
                //update converging area every time. This will allow for in flight changes of the hops plans to spread to other hops
                Geometry convergingGeom = convergeMeetingArea(theHost);
                hsc.contactArea = convergingGeom;
            }
            else {
                _log.info(TAG + "Contact with hostfrom group:"+theHost.getGroup()+" which is not in usual contact group");
            }
        }
    }

    /**
     *
     * @param dxdy the next step
     * @param ht the host task against which to check the current movement
     * @return true of the task lies in the direction of the current motion. false otherwise
     */
    protected boolean isInGeneralDirectionOfMovement(DoubleTuple dxdy, ContactNodeTask ht)
    {
        Coord l1 = this.host.getLocation();
        Coord l2 = GeometryUtil.getClosestEdge(ht.expectedLoc, l1);

        Vector2D v1 = new Vector2D(dxdy.pos1, dxdy.pos2);
        Vector2D v2 = new Vector2D(l2.getX()-l1.getX(), l2.getY()-l1.getY());
        double deg = Math.toDegrees( v1.angleTo(v2));

        return (deg < 90 && deg > -90);
    }

    protected Geometry convergeMeetingArea(DTNHost host)
    {
        Geometry g1 = CachedMapFeatureStore.Instance().getFeature(groupSetting.surveyedRegion).feature;
        Geometry g2 = getSurveyRegion(host);

        return GeometryUtil.intersectGeometry(g1,g2);
    }

    public Geometry getSurveyRegion(DTNHost host)
    {

        String featureId = XMLConfigurationReaderWriter.Instance().configuration.hostGroups
                .stream().filter(s -> s.groupId == host.getGroup()).findFirst().get().surveyedRegion;
        return CachedMapFeatureStore.Instance().getFeature(featureId).feature;
    }

    protected HashMap<DTNHost, Coord> nodesInContact(Coord currLoc, DoubleTuple dxdy)
    {
        HashMap<DTNHost, Coord> contactedHost = new HashMap<>();
        HashMap<DTNHost, Coord> hosts =  getAllHostLocations(new LinkedList<>(Arrays.asList(this.host.getName())));
        Coordinate[] line= new Coordinate[2];
        line[0] = GeometryUtil.funcCoordToCoordinate.apply(currLoc);
        line[1] = GeometryUtil.funcCoordToCoordinate.apply(new Coord(currLoc.getX()+dxdy.pos1, currLoc.getY()+dxdy.pos2));
        Geometry path = GeometryUtil.GEOMETRY_FACTORY.createLineString(line);
        for (Map.Entry<DTNHost, Coord> et: hosts.entrySet())
        {
            if(GeometryUtil.getDistance(path, GeometryUtil.funcCordToPoint.apply( et.getValue())) < maximumTransmitRange)
            {
                contactedHost.put(et.getKey(), et.getValue());
            }
        }
        return contactedHost;
    }

    //creates meeting schedule for hops that it is explicitly aware of
    private void bootstrapNeighbors()
    {
        ArrayList<String> bootstrapGrp = groupSetting.bootstrapGroups;

        if(null !=bootstrapGrp && bootstrapGrp.size() > 0)
        {
            getAllHosts();
            for (Map.Entry<String, DTNHost> entry: neighborHosts.entrySet())
            {
                DTNHost h = entry.getValue();
                String grp = h.getGroup();
                if(bootstrapGrp.contains(grp))
                {
                    if(null != groupSetting.hopNodeGroups && groupSetting.hopNodeGroups.contains(grp))
                    {
                        HopContactSchedule hsc = new HopContactSchedule(h.getName(), groupSetting.hopContactInterval, null);
                        contactHopSchedule.put(h.getName(), hsc);
                        Geometry convergingGeom = convergeMeetingArea(h);
                        hsc.contactArea = convergingGeom;
                    }
                    else if(null != groupSetting.groundNodeGroups && groupSetting.groundNodeGroups.contains(grp))
                    {
                        NodeContactSchedule nsc = new NodeContactSchedule(h.getName(), groupSetting.groundNodeGroupsContactInterval, null);
                        contactNodeSchedule.put(h.getName(), nsc);
                        Geometry convergingGeom = convergeMeetingArea(h);
                        nsc.contactArea = convergingGeom;
                    }
                }
            }
        }

        hasBootstrapped =true;
    }

    private ContactNodeTask getOverdueHopContact()
    {
        ContactNodeTask possibleTk = null;
        for (HostTask tk:queuedTasks)
        {
            if(tk instanceof ContactNodeTask)
            {
                ContactNodeTask cntk = (ContactNodeTask)tk;
                if(contactHopSchedule.containsKey(cntk.target))
                {
                    HopContactSchedule hsc =  contactHopSchedule.get(cntk.target);
                    double tippingPt;
                    if(hsc.interval.pos2 - hsc.interval.pos1 <= 0.0)
                        tippingPt = hsc.interval.pos2;
                    else
                    {
                        double pb = rng.nextDouble() * (hsc.interval.pos2 - hsc.interval.pos1);
                        tippingPt = hsc.interval.pos2 + pb;
                    }

                    if(SimClock.getTime() - hsc.lastContactAttemptAt > tippingPt)
                    {
                        possibleTk = (ContactNodeTask)tk;
                        break;
                    }
                }
            }
        }

        return possibleTk;
    }

    private boolean informRemoteOfOverdueContact(ContactNodeTask tk)
    {
        if(null == tk)
            return false;
        HashMap<String, DTNHost> hosts = getAllHosts();
        DTNHost remote = hosts.get(tk.target);
        return remote.OverDueContact(this.host, tk);
    }

    public boolean OverDueContact(DTNHost remote, ContactNodeTask tk)
    {
        //if not already another node has started contact procedure for an overdue contact
        if(remoteOverdueInitiated == null)
        {
            remoteOverdueInitiated = new ContactNodeTask(TaskType.CONTACT_HOST, HostTask.getHashId(), remote.getName(), tk.expectedLoc, tk.duration);
            return true;
        }
        else
            return false;
    }

    private void readjustDuration(ContactNodeTask tk)
    {
        Coord currLoc = host.getLocation();
        Coord target = GeometryUtil.getClosestEdge(tk.expectedLoc, currLoc);
        double dist = currLoc.distance(target);
        double time = 0;
        if(tk.expectedLoc instanceof Point){
            time = dist/ groupSetting.speed.pos2;
        }
        else {
            time = (2*dist)/ (groupSetting.speed.pos1 + groupSetting.speed.pos2);
        }
        tk.duration += time;
    }
}
