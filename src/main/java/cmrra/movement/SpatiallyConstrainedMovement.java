package cmrra.movement;

/**
 * Created by rabbiddog on 13.07.17.
 */

import cmrra.configuration.XMLConfigurationReaderWriter;
import cmrra.entity.config.Group;
import core.Coord;
import core.Settings;
import core.SimError;
import input.WKTMapReader;
import movement.MovementModel;
import movement.Path;
import movement.SwitchableMovement;
import movement.map.MapNode;
import movement.map.SimMap;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * SpatiallyConstrainedMovement give out path that is more or less constrained in a space.
 * The boundaries are not hard boundaries and allow some leeway.
 * The constrained space can contain other child spaces. movement model of the child spaces can be configured separately
 * indivudually
 */
public class SpatiallyConstrainedMovement extends MovementModel{

    /** sim map for the model */
    private SimMap map = null;
    /** node where the last path ended or node next to initial placement */
    protected MapNode lastMapNode;
    /**  max nrof map nodes to travel/path */
    //protected int maxPathLength = 100;
    /**  min nrof map nodes to travel/path */
    //protected int minPathLength = 10;
    /** May a node choose to move back the same way it came at a crossing */
    protected boolean backAllowed;
    /** map based movement model's settings namespace ({@value})*/
    public static final String SPACE_CONSTRAINT_MOVEMENT_NS = "SpatiallyConstrainedMovement";
    /** number of map files -setting id ({@value})*/
    public static final String NROF_FILES_S = "nrofMapFiles";
    /** map file -setting id ({@value})*/
    public static final String FILE_S = "mapFile";

    /** map cache -- in case last mm read the same map, use it without loading*/
    private static SimMap cachedMap = null;
    /** names of the previously cached map's files (for hit comparison) */
    private static List<String> cachedMapFiles = null;
    /** how many map files are read */
    private int nrofMapFilesRead = 0;


    protected Group groupSetting;
    protected String TAG = "Class: SpatiallyConstrainedMovement.";

    public SpatiallyConstrainedMovement()
    {super();}

    public SpatiallyConstrainedMovement(SpatiallyConstrainedMovement mm)
    {super(mm);}

    // called for each new movement model object created for a DTNHost
    public void lateInit(String groupId)
    {
        super.lateInit(groupId);
        groupSetting = XMLConfigurationReaderWriter.Instance().configuration.hostGroups
                .stream().filter(s -> s.groupId == groupId).findFirst().get();
        TAG =TAG+"host: "+host.getName()+" ";
    }

    @Override
    public Path getPath() {
        return null;
    }

    @Override
    public Coord getInitialLocation() {
        return null;
    }

    @Override
    public MovementModel replicate() {
        return new SpatiallyConstrainedMovement(this);
    }
}
