package cmrra.movement;

import cmrra.configuration.XMLConfigurationReaderWriter;
import cmrra.entity.*;
import cmrra.entity.config.DoubleTuple;
import cmrra.entity.config.Group;
import cmrra.entity.config.MapLayout;
import cmrra.utility.geometry.GeometryUtil;
import com.vividsolutions.jts.geom.*;
import core.Coord;
import core.Settings;
import core.SimClock;
import core.SimError;
import movement.MovementModel;
import movement.Path;
import movement.SwitchableMovement;
import movement.map.MapNode;
import movement.map.SimMap;

import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.*;
import java.util.function.Predicate;

/**
 * Created by rabbiddog on 13.07.17.
 */

/**
 * Movement model that follows a scheduled movement between nodes or regions.
 * The order of visiting the nodes/regions are provides externally.
 * Movement motion pattern while moving from one node/region to another is provide externally
 * Movement motion pattern inside a region is handed over to separate movement model, also provided externally
 * */
public class ScheduledMapMovement extends MovementModel{


    protected static final GeometryFactory GEOMETRY_FACTORY = new GeometryFactory();
    protected Group groupSetting;
    protected String TAG = "Class: ScheduledMapMovement. ";

    //stores the featureIds of the known stations in the surveyed region for quick retrieval
    protected ArrayList<String> featureIdStations;
    protected ConcurrentHashMap<String, MapFeature> featureStations;

    /**
     * Creates a new ScheduledMapMovement object that handles the schedule for the nodes in its map
     * Appropriately handovers the movement model for child maps to the configured movement model
    */
    public ScheduledMapMovement()
    {
        super();
    }

    // called for each new movement model object created for a DTNHost
    public void lateInit(String groupId)
    {
        if(null == host)
        {
            _log.error(TAG + "");
            throw new SimError(TAG +"Host not set yet for this movement model. Call setHost() before lateInit()");
        }
        super.lateInit(groupId);
        groupSetting = XMLConfigurationReaderWriter.Instance().configuration.hostGroups
                .stream().filter(s -> s.groupId == groupId).findFirst().get();
        TAG =TAG+"host: "+host.getName()+" ";

    }

    protected ScheduledMapMovement(ScheduledMapMovement proto)
    {
        super(proto);
        featureIdStations = new ArrayList<>();
        featureStations = new ConcurrentHashMap<>();
    }

    @Override
    public Path getPath() {
        return null;
    }

    @Override
    public Coord getInitialLocation() {
        return null;
    }

    @Override
    public MovementModel replicate() {
        return new ScheduledMapMovement(this);
    }

}
