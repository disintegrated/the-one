package cmrra.movement;

import cmrra.configuration.XMLConfigurationReaderWriter;
import cmrra.entity.config.Group;
import core.Coord;
import core.DTNHost;
import movement.MovementModel;
import movement.Path;

/**
 * Created by rabbiddog on 18.07.17.
 */
public class ProbabilisticMovement extends MovementModel{

    protected Group groupSetting;
    protected String TAG = "Class: ProbabilisticMovement.";

    public ProbabilisticMovement()
    {
        super();
    }

    public ProbabilisticMovement(ProbabilisticMovement mm)
    {
        super(mm);
         /*right now no values need to be replicated*/
    }

    // called for each new movement model object created for a DTNHost
    public void lateInit(String groupId)
    {
        super.lateInit(groupId);
        groupSetting = XMLConfigurationReaderWriter.Instance().configuration.hostGroups
                .stream().filter(s -> s.groupId == groupId).findFirst().get();
        TAG =TAG+"host: "+host.getName()+" ";
    }

    @Override
    public Path getPath() {
        return null;
    }

    @Override
    public Coord getInitialLocation() {
        return null;
    }

    @Override
    public MovementModel replicate() {
        return new ProbabilisticMovement(this);
    }

}
