package cmrra.movement;

import cmrra.configuration.XMLConfigurationReaderWriter;
import cmrra.entity.*;
import cmrra.entity.config.ComInterface;
import cmrra.entity.config.DoubleTuple;
import cmrra.entity.config.Group;
import cmrra.utility.geometry.GeometryUtil;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;
import core.Coord;
import core.DTNHost;
import movement.MovementModel;
import movement.Path;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by rabbiddog on 13.08.17.
 */
public class StraightToWaypointMovement extends MovementModel {

    protected Group groupSetting;

    protected double maximumTransmitRange = 0.0;
    public String TAG = "Class: StraightToWaypointMovement.";
    public StraightToWaypointMovement()
    {
        super();
    }

    public StraightToWaypointMovement(StraightToWaypointMovement mm)
    {
        super(mm);
    }

    public void lateInit(String groupId)
    {
        super.lateInit(groupId);
        groupSetting = XMLConfigurationReaderWriter.Instance().configuration.hostGroups
                .stream().filter(s -> s.groupId == groupId).findFirst().get();
        TAG =TAG+"host: "+host.getName()+" ";

        for (ComInterface cI:XMLConfigurationReaderWriter.Instance().configuration.comInterfaces)
        {
            if(maximumTransmitRange==0.0)
                maximumTransmitRange = cI.transmitRange;
            else if(maximumTransmitRange< cI.transmitRange)
            {
                maximumTransmitRange =cI.transmitRange;
            }
        }
    }
    @Override
    public Path getPath() {
        return null;
    }

    @Override
    public Coord getInitialLocation() {
        return null;
    }

    @Override
    public MovementModel replicate() {
        return new StraightToWaypointMovement(this);
    }

    public DoubleTuple takeNextStep(double timeIncrement, ContactNodeTask ct)
    {
        ct.taskState = HostTaskState.RUNNING;
        double dx=0.0,dy=0.0;
        Coord dest = GeometryUtil.funcPointToCord.apply((Point)ct.expectedLoc)  ;
        Coord currLoc = this.host.getLocation();
        double dist = currLoc.distance(dest);

        double possibleMovement = timeIncrement * groupSetting.speed.pos2; //at max speed
        if(possibleMovement >= dist) {
            dx = (dest.getX() -
                    currLoc.getX());
            dy = (dest.getY() -
                    currLoc.getY());
        }
        else {
            dx = (possibleMovement/dist) * (dest.getX() -
                    currLoc.getX());
            dy = (possibleMovement/dist) * (dest.getY() -
                    currLoc.getY());
            //if reached the location then stay there till the task specifies
            if((dist - possibleMovement) < 0.0009)
            {
                if(ct.duration < timeIncrement)
                {
                    ct.setDone();
                    ct.duration = 0.0;
                }
                else {
                    ct.duration -= timeIncrement;
                }
            }
        }

        return new DoubleTuple(dx,dy);
    }

    public DoubleTuple takeNextStep(double timeIncrement, GoHomeTask gt)
    {
        gt.taskState = HostTaskState.RUNNING;
        double dx=0.0,dy=0.0;
        Coord dest = gt.home;
        Coord currLoc = this.host.getLocation();
        double possibleMovement = timeIncrement * groupSetting.speed.pos2; //at max speed
        double distance = currLoc.distance(dest);
        if (possibleMovement >= distance) {
            dx = (dest.getX() -
                    currLoc.getX());
            dy = (dest.getY() -
                    currLoc.getY());
            gt.setDone();
        }
        else{
            dx = (possibleMovement/distance) * (dest.getX() -
                    currLoc.getX());
            dy = (possibleMovement/distance) * (dest.getY() -
                    currLoc.getY());

            if((distance - possibleMovement) < 0.0009)
            {
                gt.setDone();
            }
        }

        return new DoubleTuple(dx,dy);
    }
}
