package cmrra.movement;

import cmrra.configuration.XMLConfigurationReaderWriter;
import cmrra.entity.*;
import cmrra.entity.config.ComInterface;
import cmrra.entity.config.DoubleTuple;
import cmrra.entity.config.Group;
import core.Coord;
import movement.MovementModel;
import movement.Path;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by rabbiddog on 02.08.17.
 * This movement class moves the host systematically over the survey region
 * Moving in a criss-cross manner till the whole region is covered.
 * Ideally it will not return to the starting position and instead allow the next task to naturally continue
 */
public class SurveyMovement extends MovementModel {

    protected Group groupSetting;
    protected String TAG = "Class: SurveyMovement. ";
    protected double maximumTransmitRange = 0.0;

    protected ConcurrentHashMap<String, SweepTask> taskHistory; //all current and paused tasks. Aborted tasks are removed


    public SurveyMovement()
    {
        super();
    }

    public SurveyMovement(SurveyMovement mm)
    {
        super(mm);
        /*right now no values need to be replicated*/
    }

    // called for each new movement model object created for a DTNHost
    public void lateInit(String groupId)
    {
        super.lateInit(groupId);
        groupSetting = XMLConfigurationReaderWriter.Instance().configuration.hostGroups
                .stream().filter(s -> s.groupId == groupId).findFirst().get();
        TAG =TAG+"host: "+host.getName()+" ";
        for (ComInterface cI:XMLConfigurationReaderWriter.Instance().configuration.comInterfaces)
        {
            if(maximumTransmitRange==0.0)
                maximumTransmitRange = cI.transmitRange;
            else if(maximumTransmitRange< cI.transmitRange)
            {
                maximumTransmitRange =cI.transmitRange;
            }
        }
    }

    /*A task should already be assigned to the Movement Model
    *On being requested for a path, the model checks the current location of host and finds a path to start/resume
    * on the task.
    * If it reaches the end of the task or is asked to abort then it sets the task as completed and returns the remaining
    * path.
    * */
    @Override
    public Path getPath()
    {
        return null;
    }

    @Override
    public Coord getInitialLocation() {
        return null;
    }

    @Override
    public MovementModel replicate() {
        return new SurveyMovement(this);
    }

    public DoubleTuple takeNextStep(double timeIncrement, SweepTask st)
    {
        try{
            if(st.path.size() == 0)
            {
                st.taskState = HostTaskState.COMPLETED;
                return new DoubleTuple(0.0,0.0);
            }
            st.taskState = HostTaskState.RUNNING;

            //if not on path then gets to the beginning of the path.
            //else continues on the path
            Coord dest = st.path.get(0);
            double dx=0.0,dy=0.0;
            Coord currLoc = this.host.getLocation();
            double dist = currLoc.distance(dest);
            double possibleMovement = timeIncrement * (groupSetting.speed.pos2+groupSetting.speed.pos1)/2; //at avg speed
            if(possibleMovement >= dist) {
                dx = (dest.getX() -
                        currLoc.getX());
                dy = (dest.getY() -
                        currLoc.getY());
                st.path.remove(0);
            }
            else {
                dx = (possibleMovement/dist) * (dest.getX() -
                        currLoc.getX());
                dy = (possibleMovement/dist) * (dest.getY() -
                        currLoc.getY());
                if((dist - possibleMovement) < 0.0009)
                {
                    //remove the dest
                    st.path.remove(0);
                }
            }

            return new DoubleTuple(dx,dy);
        }catch (Exception ex)
        {
            _log.error(TAG + "takeNextStep() threw error. Message: "+ex.getMessage()+". Stack: "+ex.getStackTrace());
            throw ex;
        }

    }
}
