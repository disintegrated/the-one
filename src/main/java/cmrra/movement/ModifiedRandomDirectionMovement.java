package cmrra.movement;

import cmrra.configuration.XMLConfigurationReaderWriter;
import cmrra.entity.*;
import cmrra.entity.config.ComInterface;
import cmrra.entity.config.DoubleTuple;
import cmrra.entity.config.Group;
import cmrra.utility.geometry.GeometryUtil;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Polygon;
import core.Coord;
import core.DTNHost;
import movement.MovementModel;
import movement.Path;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by rabbiddog on 18.07.17.
 */
public class ModifiedRandomDirectionMovement extends MovementModel{

    protected Group groupSetting;
    protected String TAG = "Class: ModifiedRandomDirectionMovement.";
    protected double maximumTransmitRange = 0.0;

    public  ModifiedRandomDirectionMovement()
    {
        super();
    }

    public ModifiedRandomDirectionMovement(ModifiedRandomDirectionMovement mm)
    {
        super(mm);
         /*right now no values need to be replicated*/
    }

    // called for each new movement model object created for a DTNHost
    public void lateInit(String groupId)
    {
        super.lateInit(groupId);
        groupSetting = XMLConfigurationReaderWriter.Instance().configuration.hostGroups
                .stream().filter(s -> s.groupId == groupId).findFirst().get();
        TAG =TAG+"host: "+host.getName()+" ";

        for (ComInterface cI:XMLConfigurationReaderWriter.Instance().configuration.comInterfaces)
        {
            if(maximumTransmitRange==0.0)
                maximumTransmitRange = cI.transmitRange;
            else if(maximumTransmitRange< cI.transmitRange)
            {
                maximumTransmitRange =cI.transmitRange;
            }
        }
    }
    @Override
    public Path getPath() {
        return null;
    }

    @Override
    public Coord getInitialLocation() {
        return null;
    }

    @Override
    public MovementModel replicate() {
        return new ModifiedRandomDirectionMovement(this);
    }

    public DoubleTuple takeNextStep(double timeIncrement, ContactNodeTask ct)
    {
        double dx=0.0,dy=0.0;
        double dist = 0.0;
        Coord currLoc = this.host.getLocation();

        if(GeometryUtil.isPointInPolygon((Polygon) ct.expectedLoc, GeometryUtil.funcCordToPoint.apply(currLoc)))
        {
            if(ct.duration < timeIncrement)
            {
                ct.taskState = HostTaskState.COMPLETED;
                ct.duration = 0.0;
            }
            else {
                ct.duration -= timeIncrement;
                ct.taskState = HostTaskState.RUNNING;
            }
        }
        //check if on its way to previously decided location
        //if not then decide on a new direction and distance to create a destination
        if(null == ct.onWayTo)
        {
            //first check if current location is inside the region
            ct.onWayTo = GeometryUtil.randomPointInPolygon((Polygon) ct.expectedLoc);
            //ct.onWayTo = GeometryUtil.modifiedRandomDirection((Polygon) ct.expectedLoc, currLoc);
        }
        dist = currLoc.distance(ct.onWayTo);
        double possibleMovement = timeIncrement * (groupSetting.speed.pos1 + groupSetting.speed.pos2)/2; //at avg speed
        if(possibleMovement >= dist) {
            dx = (ct.onWayTo.getX() -
                    currLoc.getX());
            dy = (ct.onWayTo.getY() -
                    currLoc.getY());
            ct.onWayTo = null;
        }
        else {
            dx = (possibleMovement/dist) * (ct.onWayTo.getX() -
                    currLoc.getX());
            dy = (possibleMovement/dist) * (ct.onWayTo.getY() -
                    currLoc.getY());
            if((dist - possibleMovement) < 0.0009)
            {
                ct.onWayTo = null;
            }
        }
        return new DoubleTuple(dx,dy);
    }

    public  DoubleTuple takeNextStep(double timeIncrement, WanderTask st)
    {
        if(st.duration < timeIncrement)
        {
            st.taskState = HostTaskState.COMPLETED;
            st.duration = 0.0;
        }
        else {
            st.duration -= timeIncrement;
            st.taskState = HostTaskState.RUNNING;
        }

        double dx=0.0,dy=0.0;
        double dist = 0.0;
        Coord currLoc = this.host.getLocation();
        if(null == st.onWayTo)
        {
            //first check if current location is inside the region
            st.onWayTo = GeometryUtil.randomPointInPolygon(st.searchArea);
        }
        dist = currLoc.distance(st.onWayTo);
        double possibleMovement = timeIncrement * (groupSetting.speed.pos2 + groupSetting.speed.pos1) /2; //at avg speed
        if(possibleMovement >= dist) {
            dx = (st.onWayTo.getX() -
                    currLoc.getX());
            dy = (st.onWayTo.getY() -
                    currLoc.getY());
            st.onWayTo = null;
        }
        else {
            dx = (possibleMovement/dist) * (st.onWayTo.getX() -
                    currLoc.getX());
            dy = (possibleMovement/dist) * (st.onWayTo.getY() -
                    currLoc.getY());
            if((dist - possibleMovement) < 0.0009)
            {
                //remove the dest
                st.onWayTo = null;
            }
        }
        return new DoubleTuple(dx,dy);
    }

}
