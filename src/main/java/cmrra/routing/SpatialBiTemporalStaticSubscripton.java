package cmrra.routing;

import cmrra.data.index.entity.SpatialAxisInterval;
import cmrra.data.index.entity.TransactionTime;
import cmrra.data.index.entity.ValidTime;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import core.Connection;
import routing.MessageRouter;

/**
 * Created by rabbiddog on 12.11.17.
 */
public class SpatialBiTemporalStaticSubscripton extends Subscription<SpatialBiTemporalStaticSubscripton> {

    private Sp2DBiTemporalBoundary _boundary;

    public SpatialBiTemporalStaticSubscripton(SpatialAxisInterval x, SpatialAxisInterval y, TransactionTime t, ValidTime v)
    {
        this._boundary = new Sp2DBiTemporalBoundary(x, y, t, v);
    }

    @Override
    public SpatialBiTemporalStaticSubscripton merge(SpatialBiTemporalStaticSubscripton sub) {
        Sp2DBiTemporalBoundary merged = _boundary.union(sub.getSubscritpionBoundary());
        return new SpatialBiTemporalStaticSubscripton(merged.X(), merged.Y(), merged.T(), merged.V());
    }

    @Override
    public Sp2DBiTemporalBoundary getSubscritpionBoundary() {
        return _boundary;
    }
}
