package cmrra.routing;

import cmrra.configuration.XMLConfigurationReaderWriter;
import cmrra.data.index.entity.Spatial2DBiTemporalData;
import cmrra.data.index.entity.SpatialAxisInterval;
import cmrra.data.index.entity.TransactionTime;
import cmrra.data.index.entity.ValidTime;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import cmrra.data.index.utility.Log;
import cmrra.entity.CachedMapFeatureStore;
import cmrra.entity.MapFeature;
import cmrra.entity.config.Group;
import cmrra.entity.config.MapLayout;
import cmrra.entity.data.ONData;
import com.google.common.collect.Sets;
import com.vividsolutions.jts.geom.Envelope;
import core.*;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import routing.MessageRouter;
import util.Tuple;

import java.util.*;
import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.stream.Collectors;

/**
 * Created by rabbiddog on 12.11.17.
 * A routing algorithm to test a simple subscription based routing algorithm
 * Currently there is no concrete algorithm to base this algorithm
 * The outline of the routing scheme is:
 *      Nodes A publish their interests in certain types of messages through a protocol message
 *      Other nodes in contact process that request and send a reply with a Summary Vector of available data for that subscription
 *      Node A checks from the summary vector which data it will receive new and sends Request vector
 *      Other node replies with a stream of transmissions with the requested data till all data are transferred or connection is lost
 *      Data should be uniquely identifiable
 */
public class SubscriptionRouter {

    private DTNHost _host;

    private boolean growSubscription;
    private boolean forceDataDump;
    private boolean prioritizeData;

    public void init(DTNHost host)
    {
        this._host = host;
        this._permanent_subscritpion = createPermanentSubscription();
        this._overLayed_subscriptions = new LinkedHashMap<>();
        this.sendingConnections = new ArrayList<>(1);
    }

    //this is the subscription that the applications on this node create.
    //it is updated only when applications on this node change their subscription
    private Subscription _permanent_subscritpion;

    //subscriptions obtained from other nodes along with their time to live in seconds
    //merge with the _permanent_subscritpion of this router to create a temporary subscrition to filter data
    private HashMap<String, Pair<Subscription, Double>> _overLayed_subscriptions;
    /** connection(s) that are currently used for sending */
    protected ArrayList<Connection> sendingConnections;
    /**
     * map of Host to Session
     * close session on
     * 1)   completion of tranfer
     * 2)   Connection loss
     */
    public HashMap<String, Session> session = new LinkedHashMap<>();

	/* Return values when asking to start a transmission:
	 * RCV_OK (0) means that the host accepts the message and transfer started,
	 * values < 0 mean that the  receiving host will not accept this
	 * particular message (right now),
	 * values > 0 mean the host will not right now accept any message.
	 * Values in the range [-100, 100] are reserved for general return values
	 * (and specified here), values beyond that are free for use in
	 * implementation specific cases */
    /** Receive return value for OK */
    public static final int RCV_OK = 0;
    /** Receive return value for busy receiver */
    public static final int TRY_LATER_BUSY = 1;
    /** Receive return value for an old (already received) message */
    public static final int DENIED_OLD = -1;
    /** Receive return value for not enough space in the buffer for the msg */
    public static final int DENIED_NO_SPACE = -2;
    /** Receive return value for messages whose TTL has expired */
    public static final int DENIED_TTL = -3;
    /** Receive return value for a node low on some resource(s) */
    public static final int DENIED_LOW_RESOURCES = -4;
    /** Receive return value for a node low on some resource(s) */
    public static final int DENIED_POLICY = -5;
    /** Receive return value for unspecified reason */
    public static final int DENIED_UNSPECIFIED = -99;

    private List<MessageListener> mListeners;

    /** applications attached to the host */
    private HashMap<String, Collection<Application>> applications = null;

    public SubscriptionRouter() {

        this.applications = new HashMap<String, Collection<Application>>();
        this.prioritizeData = XMLConfigurationReaderWriter.Instance().configuration.prioritizeData;
        this.forceDataDump = XMLConfigurationReaderWriter.Instance().configuration.prioritizeData;
    }

    /**
     * Copy-constructor.
     * @param r Router to copy the settings from.
     */
    protected SubscriptionRouter(SubscriptionRouter r) {

        this.applications = new HashMap<String, Collection<Application>>();
        for (Collection<Application> apps : r.applications.values()) {
            for (Application app : apps) {
                addApplication(app.replicate());
            }
        }

        this._overLayed_subscriptions = new LinkedHashMap<>();
        this.forceDataDump = r.forceDataDump;
        this.prioritizeData = r.prioritizeData;
    }

    /**
     * Updates router.
     * This method should be called (at least once) on every simulation
     * interval to update the status of transfer(s).
     */
    public void update(){
       for (Collection<Application> apps : this.applications.values()) {
            for (Application app : apps) {
                app.update(this._host); //this is where the applicatins do their data collection
            }
       }

       try{

           //get data already here if imitating complete data transfer at each meet
           //this will bypass the normal transfer of data through the network interface
           if(forceDataDump)
           {
               List<Connection> connections = getConnections();

               if (connections.size() == 0) {
                   return;
               }
               for (Connection conn: connections)
               {
                    if(conn.isUp())
                    {
                        DTNHost remote = conn.getOtherNode(_host);
                        Session s = this.session.get(remote.getName());

                        //if a session was created then the request vector will have been generated
                        if(null != s)
                        {
                            /*if((SimClock.getTime() - s.lastForceUpdate ) < 60)//60sec gap for forced update.Not the most elegant soln.
                                continue;*/
                            s.lastForceUpdate = SimClock.getTime();
                            if(s.localrequestVector.isEmpty())
                                s.update();
                            ArrayList<Spatial2DBiTemporalData<String>> dataDump = s.remote.requestDataDump(_host);
                            for (Spatial2DBiTemporalData<String> data : dataDump)
                            {
                                s.receiveData(data);
                            }
                        }
                    }
               }

           }
           else {

           /* in theory we can have multiple sending connections even though
		   currently all routers allow only one concurrent sending connection */
               for (int i=0; i<this.sendingConnections.size(); ) {
                   boolean removeCurrent = false;
                   Connection con = sendingConnections.get(i);

			/* finalize ready transfers */
                   if (con.isDataTransferred()) {
                       if (con.getData() != null) {
                           con.finalizeTransferData();
                       } /* else: some other entity aborted transfer */
                       removeCurrent = true;
                   }
			/* remove connections that have gone down */
                   else if (!con.isUp()) {
                       if (con.getData() != null) {
                           con.abortTransferData();
                       }
                       removeCurrent = true;
                   }

                   if (removeCurrent) {
                       sendingConnections.remove(i);
                   }
                   else {
				/* index increase needed only if nothing was removed */
                       i++;
                   }
               }

               if (isTransferring() || !canStartTransfer()) {
                   return; // transferring, don't try other connections yet
               }

               exchangeDeliverableData();
           }
       }catch (Exception ex)
       {
           Log.error("error in update() at host:"+_host.getName()+Log.getExceptionString(ex));
           throw new SimError("error in update() at host:"+_host.getName()+Log.getExceptionString(ex));
       }
    }

    /**
     * Exchanges deliverable (to final recipient) messages between this host
     * and all hosts this host is currently connected to. First all messages
     * from this host are checked and then all other hosts are asked for
     * messages to this host. If a transfer is started, the search ends.
     * @return A connection that started a transfer or null if no transfer
     * was started
     */
    protected Connection exchangeDeliverableData() {
        List<Connection> connections = getConnections();

        if (connections.size() == 0) {
            return null;
        }

        @SuppressWarnings(value = "unchecked")
        Tuple<Spatial2DBiTemporalData, Connection> t =
                tryDataForConnected(getConnectedWithData());

        if (t != null) {
            return t.getValue(); // started transfer
        }

        // didn't start transfer to any node -> ask messages from connected
        for (Connection con : connections) {
            if (con.getOtherNode(getHost()).requestDeliverableData(con)) {
                return con;
            }
        }

        return null;
    }

    /**
     * Gets a randomly shuffled list of connections that
     * @return
     */
    protected List<Connection> getConnectedWithData()
    {
        List<Connection> cons = _host.getConnections();

        List<Connection> consWithData = cons.stream().filter(c -> {
            DTNHost other = c.getOtherNode(_host);
            Session s = this.session.get(other.getName());
            if(null == s)
                return false;
            else if(s.canStartTransfer())
                return true;
            else
                return false;
        }).collect(Collectors.toList());

        Collections.shuffle(consWithData, new Random(SimClock.getIntTime()));
        return consWithData;
    }

    /**
     * Tries to send data for the connections that are mentioned
     * in the list in the order they are in the list until one of
     * the connections starts transferring or all tuples have been tried.
     * @param consWithData The connections that have pending data to send
     * @return The tuple whose connection accepted the message or null if
     * none of the connections accepted the message that was meant for them.
     */
    protected Tuple<Spatial2DBiTemporalData, Connection> tryDataForConnected(
            List<Connection> consWithData) {
        if (consWithData.size() == 0) {
            return null;
        }

        for (Connection conn : consWithData) {
            Session s = this.session.get(conn.getOtherNode(_host).getName());
            if(null == s)
                continue;

            Spatial2DBiTemporalData<String> data = s.requestDeliverableData();
            if (startTransferData(conn, data) == RCV_OK) {
                s.dataDelivered(data.UUID);
                return new Tuple<>(data, conn);
            }
        }

        return null;
    }

    /**
     * Returns true if this router is transferring something at the moment or
     * some transfer has not been finalized.
     * @return true if this router is transferring something
     */
    public boolean isTransferring() {
        if (this.sendingConnections.size() > 0) {
            return true; // sending something
        }

        List<Connection> connections = getConnections();

        if (connections.size() == 0) {
            return false; // not connected
        }

        for (int i=0, n=connections.size(); i<n; i++) {
            Connection con = connections.get(i);
            if (!con.isReadyForTransferData()) {
                return true;	// a connection isn't ready for new transfer
            }
        }

        return false;
    }

    /**
     * Makes rudimentary checks (that we have at least one message and one
     * connection) about can this router start transfer.
     * @return True if router can start transfer, false if not
     */
    protected boolean canStartTransfer() {
        Optional<Session> s = session.values().stream().filter(ss -> ss.canStartTransfer()).findFirst();
        return s.isPresent();
    }

    /**
     * Returns a list of connections this host currently has with other hosts.
     * @return a list of connections this host currently has with other hosts
     */
    protected List<Connection> getConnections() {
        return getHost().getConnections();
    }

    /**
     * Informs the router about change in connections state.
     * Not clear if connections are created for lifetime. If yes then definitely need to
     * teardown sessions between long periods of connection down.
     * @param con The connection that changed
     */
    public void changedConnection(Connection con)
    {
        if(con.isUp())
        {
            DTNHost remote = con.getOtherNode(_host);
            Session s = this.session.get(remote.getName()); //just to be safe
            if(null == s)
            {
                Session newSession = new Session(remote);
                this.session.put(remote.getName(), newSession);
                remote.OpenSession(_host);
                newSession.Open();
            }
            else {
                s.update();
            }

        }
    }

    public void OpenSession(DTNHost remote)
    {
        Session s = this.session.get(remote.getName()); //just to be safe
        if(null == s)
        {
            Session newSession = new Session(remote);
            this.session.put(remote.getName(), newSession);
            newSession.Open();
        }
    }

    /**
     * Returns the host this router is in
     * @return The host object
     */
    protected DTNHost getHost() {
        return this._host;
    }

    /**
     * Start sending a message to another host.
     * @param id Id of the message to send
     * @param to The host to send the message to
     */
   /* public void sendData(String id, DTNHost to) {
        Message m = getMessage(id);
        Message m2;
        if (m == null) throw new SimError("no message for id " +
                id + " to send at " + this.host);

        m2 = m.replicate();	// send a replicate of the message
        to.receiveMessage(m2, this.host);
    }
*/
    /**
     * Requests for deliverable message from this router to be sent trough a
     * connection.
     * @param con The connection to send the messages trough
     * @return True if this router started a transfer, false if not
     */
    public boolean requestDeliverableData(Connection con) {
        if (isTransferring()) {
            return false;
        }

        DTNHost other = con.getOtherNode(getHost());
        Session s = session.get(other.getName());
        if(null == s || !s.canStartTransfer())
            return false;

        Spatial2DBiTemporalData<String> data = s.requestDeliverableData();
        if(startTransferData(con, data) == RCV_OK)
        {
            s.dataDelivered(data.UUID);
            return true;
        }

        return false;
    }

    /**
     * Tries to start a transfer of message using a connection. Is starting
     * succeeds, the connection is added to the watch list of active connections
     * @param con The connection to use
     * @return the value returned by
     * {@link Connection#startTransferData(DTNHost, Spatial2DBiTemporalData<String>)}
     */
    protected int startTransferData(Connection con, Spatial2DBiTemporalData<String> data) {
        int retVal;

        //make a copy of the data

        if (!con.isReadyForTransfer()) {
            return TRY_LATER_BUSY;
        }
        try{
            retVal = con.startTransferData(getHost(), data);
            if (retVal == RCV_OK) { // started transfer
                addToSendingConnections(con);
            }

            return retVal;
        }catch (Exception ex)
        {
            Log.error("Could not start transfer because of error "+Log.getExceptionString(ex));
            return MessageRouter.DENIED_UNSPECIFIED;
        }
    }

    /*
     * Adds a connections to sending connections which are monitored in
     * the update.
     * @see #update()
     * @param con The connection to add
     */
    protected void addToSendingConnections(Connection con) {
        this.sendingConnections.add(con);
    }

    /**
     * Try to start receiving a Spatio-Temporal data from another host.
     * @param d Spatio-Temporal data
     * @param from Who the message is from
     * @return Value zero if the node accepted the message (RCV_OK), value less
     * than zero if node rejected the message (e.g. DENIED_OLD), value bigger
     * than zero if the other node should try later (e.g. TRY_LATER_BUSY).
     */
    public int receiveData(Spatial2DBiTemporalData<String> d, DTNHost from) {

        //find session and send message there
        Session s = session.get(from.getName());
        if(null != s)
            s.receiveData(d);

        return RCV_OK; // data was received on the router. Ignore if added to the index
    }

    public Set<String> provideSummaryVector(Sp2DBiTemporalBoundary boundary)
    {
         io.reactivex.Observable<Spatial2DBiTemporalData<String>>  ob=  _host.store().search(boundary);

        final Set<String>[] records = new Set[1];

        DisposableObserver<Spatial2DBiTemporalData<String>> observer
                = new DisposableObserver<Spatial2DBiTemporalData<String>>() {
            Set<String> r = new HashSet<>();
            @Override
            public void onNext(Spatial2DBiTemporalData<String> data) {
                if(!data.isDeleted())
                    r.add(data.UUID);
            }

            @Override
            public void onError(Throwable e) {
                Log.error("Error occured trying to generate summary vector at Host "+_host.getName());
                r = null;
                this.dispose();
            }

            @Override
            public void onComplete() {
                records[0] = r;
            }
        };

        ob.subscribe(observer);
        if(!observer.isDisposed())
            observer.dispose();

        return records[0];
    }

    public void receiveRequestVector(DTNHost remote, HashSet<String> v)
    {
        Session s = session.get(remote.getName());
        if(null != s) //connection was not closed
        {
            s.receiveRequestVector(v);
        }
    }

    /**
     * Deletes a message from the buffer and informs message listeners
     * about the event
     * @param id Identifier of the message to delete
     * @param drop If the message is dropped (e.g. because of full buffer) this
     * should be set to true. False value indicates e.g. remove of message
     * because it was delivered to final destination.
     */
    /*public void deleteMessage(String id, boolean drop) {
        Message removed = removeFromMessages(id);
        if (removed == null) throw new SimError("no message for id " +
                id + " to remove at " + this.host);

        for (MessageListener ml : this.mListeners) {
            ml.messageDeleted(removed, this.host, drop);
        }
    }*/

    /**
     * Adds an application to the attached applications list.
     *
     * @param app	The application to attach to this router.
     */
    public void addApplication(Application app) {
        if (!this.applications.containsKey(app.getAppID())) {
            this.applications.put(app.getAppID(),
                    new LinkedList<Application>());
        }
        this.applications.get(app.getAppID()).add(app);
    }

    /**
     * Returns all the applications that want to receive messages for the given
     * application ID.
     *
     * @param ID	The application ID or <code>null</code> for all apps.
     * @return		A list of all applications that want to receive the message.
     */
 /*   public Collection<Application> getApplications(String ID) {
        LinkedList<Application>	apps = new LinkedList<Application>();
        // Applications that match
        Collection<Application> tmp = this.applications.get(ID);
        if (tmp != null) {
            apps.addAll(tmp);
        }
        // Applications that want to look at all messages
        if (ID != null) {
            tmp = this.applications.get(null);
            if (tmp != null) {
                apps.addAll(tmp);
            }
        }
        return apps;
    }*/

    /**
     * Creates a replicate of this router. The replicate has the same
     * settings as this router but empty buffers and routing tables.
     * @return The replicate
     */
    public SubscriptionRouter replicate()
    {
        return new SubscriptionRouter(this);
    }

    /**
     * Returns a String presentation of this router
     * @return A String presentation of this router
     */
    public String toString() {
        return getClass().getSimpleName() + " of " +
                this.getHost().toString() ;
    }

    /**
     * returns a subscription that was designed for this node
     * @return Currently a Sp2BiTemporalDynamicSubscription
     */
    public Subscription createPermanentSubscription()
    {
        Group groupSetting = XMLConfigurationReaderWriter.Instance().configuration.hostGroups
                .stream().filter(s -> s.groupId == _host.getGroup()).findFirst().get();
        MapLayout layout = XMLConfigurationReaderWriter.Instance().configuration.mapLayouts.stream()
                .filter(s -> s.featureId.compareTo(groupSetting.surveyedRegion) == 0).findFirst().get();
        MapFeature st = CachedMapFeatureStore.Instance().getFeature(layout.featureId);
        growSubscription = groupSetting.growSubscription;
        Envelope env = st.feature.getEnvelopeInternal();
        SpatialAxisInterval x = new SpatialAxisInterval(env.getMinX(), env.getMaxX());
        SpatialAxisInterval y = new SpatialAxisInterval(env.getMinY(), env.getMaxY());

        if(groupSetting.subscriptionType.type == 1)
        {
            return new Sp2BiTemporalDynamicSubscription(x, y, groupSetting.subscriptionType.t_start, groupSetting.subscriptionType.v_start);
        }

            else
        {
            TransactionTime t = cmrra.data.index.factories.Geometryfactory.createTransactionTime(groupSetting.subscriptionType.t_start, groupSetting.subscriptionType.t_end);
            ValidTime v = cmrra.data.index.factories.Geometryfactory.createValidTime(groupSetting.subscriptionType.v_start, groupSetting.subscriptionType.v_end);
            return new SpatialBiTemporalStaticSubscripton(x, y, t,v);
        }

    }

    public Subscription getOverArchSubscription()
    {
        Subscription merged = _permanent_subscritpion;
        if(growSubscription)
        {
            for (Pair<Subscription, Double> sub: _overLayed_subscriptions.values())
            {
                merged = merged.merge(sub.getLeft()); //the type of subscription created is dependent on the type of _permanent_subscritpion
            }
        }
        return merged;
    }

    public ArrayList<Spatial2DBiTemporalData<String>> requestDataDump(DTNHost remote)
    {
        Session s = session.get(remote.getName());
        if(null != s) //connection was not closed
        {
            return s.requestDataDump();
        }
        else {
            return new ArrayList<>();
        }
    }

    private final class Session{

        public String sessionID;
        private Subscription remoteSubscription;
        private ArrayBlockingQueue<Subscription> localSubscription;
        public final DTNHost remote;
        public double asleep_since;
        public double lastForceUpdate;

        private HashSet<String> localrequestVector;
        private LinkedList<String> remoteRequestVector;
        public Session(DTNHost remote)
        {
            sessionID = UUID.randomUUID().toString();
            this.remote = remote;
            localrequestVector = new HashSet<>();
            asleep_since = SimClock.getIntTime();
            lastForceUpdate = 0; //use only when forced update is used
        }

        public void Open()
        {
            update();
        }
        public void update()
        {
            try{
                remoteSubscription = remote.getOverArchSubscription();
                _overLayed_subscriptions.put(remote.getName(), new ImmutablePair<Subscription, Double>(remoteSubscription, SimClock.getTime())); //will be used only if subscription of this node grows
                localSubscription = generateLocalSubscription();
                fakeHandshake();
            }catch (Exception ex)
            {
                Log.error("error in Session update in host:"+_host.getName() + " for remote:"+remote.getName()+ " "+ Log.getExceptionString(ex));
                throw new SimError("error in Session update in host:"+_host.getName() + " for remote:"+remote.getName()+ " "+ Log.getExceptionString(ex));
            }
        }

        public void fakeHandshake()
        {
            while (localSubscription.peek() != null)
            {
                Sp2DBiTemporalBoundary b = localSubscription.poll().getSubscritpionBoundary();
                Set<String> summary = remote.provideSummaryVector(b);
                if(null == summary)
                {
                    Log.error(remote.getName()+" ran into error in provideSummaryVector()");
                    continue;
                }
                Set<String> localSummary = _host.provideSummaryVector(b);
                if(null == localSummary)
                {
                    Log.error(_host.getName()+" ran into error in local provideSummaryVector()");
                    continue;
                }
                 List<String> diff = Sets.difference(summary, localSummary).stream().collect(Collectors.toList());
                for (String d:diff)
                {
                    localrequestVector.add(d);
                }
            }

            remote.receiveRequestVector(_host, localrequestVector);
        }

        public void receiveRequestVector(HashSet<String> v)
        {
            remoteRequestVector = new LinkedList<>(v); //copy the contents
        }

        public void receiveData(Spatial2DBiTemporalData<String> data)
        {
            try {
                /*if(_host.getGroup().equals("g11") || _host.getGroup().equals("g12") || _host.getGroup().equals("g13") ||
                        _host.getGroup().equals("g14") || _host.getGroup().equals("g15") || _host.getGroup().equals("g16")
                        || _host.getGroup().equals("g17") || _host.getGroup().equals("g18") || _host.getGroup().equals("g19"))
                {
                    Log.info("data received");
                }*/
                if (localrequestVector.contains(data.UUID))
                {
                    ONData d = (ONData) data;
                    d.addToPath(_host.getName()); //keeps track of the nodes this data passed through
                    _host.store().add(data);
                }
                else {
                    Log.error("received data not int list");
                }
            }
            catch (Exception ex)
            {
                Log.error("Data with ID:"+data.UUID+" could not be stored in DB because of the following error "+Log.getExceptionString(ex));
            }
            finally {
                localrequestVector.remove(data.UUID); //will return false if data was never requested
            }
        }

        public void dataDelivered(String UUID)
        {
            remoteRequestVector.remove(UUID);
        }

        public Spatial2DBiTemporalData<String> requestDeliverableData()
        {
            //check if the request vector is old
            if((SimClock.getIntTime() - asleep_since) > 1800) //randomly selected 1/2 hr limit
                update();

            asleep_since = SimClock.getIntTime();
            Spatial2DBiTemporalData<String>[] data = new Spatial2DBiTemporalData[1];

            while (null == data[0] && remoteRequestVector.size() > 0)
            {
                String UUID = remoteRequestVector.peek();
                Single<com.google.common.base.Optional<Spatial2DBiTemporalData<String>>> single =_host.store().searchByID(UUID);
                DisposableSingleObserver<com.google.common.base.Optional<Spatial2DBiTemporalData<String>>> observer = new DisposableSingleObserver<com.google.common.base.Optional<Spatial2DBiTemporalData<String>>>() {
                    @Override
                    public void onSuccess(com.google.common.base.Optional<Spatial2DBiTemporalData<String>> sp2data) {
                        if(sp2data.isPresent())
                        {
                            Spatial2DBiTemporalData<String> d = sp2data.get();
                            if(!d.isDeleted())
                                data[0] = d;
                            else
                                remoteRequestVector.poll(); //remove the incorrect entry
                        }
                        else
                            remoteRequestVector.poll(); //remove the incorrect entry
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.error("Error encountered while searching for data by Id: "+UUID+" at host:"+ _host.getName());
                    }
                };

                single.subscribeWith(observer);
                observer.dispose();
            }


            return data[0];
        }

        public ArrayList<Spatial2DBiTemporalData<String>> requestDataDump()
        {
            ArrayList<Spatial2DBiTemporalData<String>> dataDump = new ArrayList<>();
            Observable<Pair<String, com.google.common.base.Optional<Spatial2DBiTemporalData<String>>>> tObservable =
                    _host.store().searchByIDs(new HashSet<>(remoteRequestVector));
            DisposableObserver<Pair<String, com.google.common.base.Optional<Spatial2DBiTemporalData<String>>>> observer =
                    new DisposableObserver<Pair<String, com.google.common.base.Optional<Spatial2DBiTemporalData<String>>>>() {
                        @Override
                        public void onNext(Pair<String, com.google.common.base.Optional<Spatial2DBiTemporalData<String>>> stringOptionalPair) {
                            if(stringOptionalPair.getRight().isPresent())
                                dataDump.add(stringOptionalPair.getRight().get());
                        }

                        @Override
                        public void onError(Throwable e) {
                            //ignore
                        }

                        @Override
                        public void onComplete() {
                            //no completion required
                        }
                    };

            tObservable.subscribe(observer);
            observer.dispose();
            remoteRequestVector.clear();
            return dataDump;
        }

        public boolean canStartTransfer()
        {
            return !(remoteRequestVector.isEmpty());
        }
    }

    /**
     * Creates a queue of subscription that this node is interedted in
     * The queue ensures the order of the data in the request vector and hence the priority of data
     * @return
     */
    public ArrayBlockingQueue<Subscription> generateLocalSubscription()
    {
        Comparator<Pair<Subscription, Double>> AGE = new Comparator<Pair<Subscription, Double>>() {
            @Override
            public int compare(Pair<Subscription, Double> o1, Pair<Subscription, Double> o2) {
                return (int)(o2.getRight() - o1.getRight()); //descending order
            }
        };
        ArrayBlockingQueue<Subscription> s;
        //prioritize by most recently seen
        if(prioritizeData)
        {
            s = new ArrayBlockingQueue<Subscription>(_overLayed_subscriptions.size()+1);
            s.add(_permanent_subscritpion);
            List<Pair<Subscription, Double>> l = new ArrayList<>(_overLayed_subscriptions.values());
            Collections.<Pair<Subscription, Double>> sort(l, AGE);

            for (Pair<Subscription, Double> p:l)
            {
                s.add(p.getLeft());
            }

            return s;
        }else {
            s = new ArrayBlockingQueue<Subscription>(1);
            s.add(getOverArchSubscription());
            return s;
        }
    }
}
