package cmrra.routing;

/**
 * Created by rabbiddog on 13.11.17.
 */
import cmrra.data.index.entity.Spatial2DBiTemporalData;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import io.reactivex.functions.Function;
public abstract class Subscription<T extends Subscription<T>> {

     public abstract T merge(T sub);

     public abstract Sp2DBiTemporalBoundary getSubscritpionBoundary();

}
