package cmrra.routing;

/**
 * Created by rabbiddog on 12.11.17.
 *
 */
import cmrra.data.index.entity.*;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import cmrra.data.index.factories.*;
import cmrra.data.index.utility.ToolKit;
import io.reactivex.functions.Function;

public class Sp2BiTemporalDynamicSubscription extends Subscription<Sp2BiTemporalDynamicSubscription> {

    public SpatialAxisInterval X() {
        return _x;
    }

    public void setX(SpatialAxisInterval _x) {
        this._x = _x;
    }

    public SpatialAxisInterval Y() {
        return _y;
    }

    public void setY(SpatialAxisInterval _y) {
        this._y = _y;
    }

    public TransactionTime T() {
        return new TransactionTime(ToolKit.currentSeconds() - transaction_time_range);
    }

    public ValidTime V() {
        return new ValidTime(ToolKit.currentSeconds() - valid_time_range);
    }

    private SpatialAxisInterval _x;
    private SpatialAxisInterval _y;
    private long transaction_time_range;
    private long valid_time_range;

    public Sp2BiTemporalDynamicSubscription(SpatialAxisInterval x, SpatialAxisInterval y, long t, long v)
    {
        _x = x;
        _y = y;
        transaction_time_range = t;
        valid_time_range = v;
    }

    public Sp2DBiTemporalBoundary getSubscriptionBoundary()
    {
        return Geometryfactory.createBoundary(X(), Y(), T(), V());
    }

    @Override
    public Sp2BiTemporalDynamicSubscription merge(Sp2BiTemporalDynamicSubscription other) {
        SpatialAxisInterval x_mergedd = _x.union(other._x);
        SpatialAxisInterval y_merged = _y.union(other._y);
        long t_merged = Math.min(transaction_time_range, other.transaction_time_range);
        long v_merged = Math.min(valid_time_range, other.valid_time_range);

        return new Sp2BiTemporalDynamicSubscription(x_mergedd, y_merged, t_merged, v_merged);
    }

    @Override
    public Sp2DBiTemporalBoundary getSubscritpionBoundary() {
        return new Sp2DBiTemporalBoundary(X(), Y(), T(), V());
    }
}
